---
show:
  - navigation
hide:
  - toc
---
#
=== "`Sujet`"
    <center>
    **Petit devoir sur la Récurrence**
    </center>
    <center>
    *Un soin particulier sera apporté à la qualité de la rédaction.*
    </center>
    <center>
    *L'usage de la calculatrice est recommandé.*
    </center>
    <center>
    *Durée : 20 minutes*
    </center>
    Soit la suite numérique définie sur l'ensemble des entiers naturels ℕ
    par :

    <center>
    $u_0=2$ et $u_{n+1}=\frac 1 5u_n+3\times 0,5^n$ pour $n>0$
    </center>

    1.  Recopier et compléter ce tableau à l'aide de la calculatrice :
        On donnera des valeurs approchées à $10^{-4}$ des termes de la suites.

        |$n$|0|1|2|3|4|5|
        |-|-|-|-|-|-|-|
        |$u_{n}$|2| | | | | |

    2.  D'après ce tableau, énoncer une conjecture sur le sens de variation
        de la suite et sur le comportement de la suite à l'infini.

    3.  Démontrer par récurrence que pour tout entier non nul, on a:
    $u_n\geqslant \frac{15} 4\times 0,5^n$.

    4.  En déduire que pour tout entier naturel non nul, $u_{n+1}-u_n\leqslant 0$ .

    5.  Conclure.

=== "`Corrigé`"
    !!! attention "Pas disponible"