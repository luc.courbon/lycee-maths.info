---
date: September 5, 2022
geometry: "left=2cm,right=2cm,top=1.3cm,bottom=1.3cm"
output: pdf_document
---
#
## Exercice 1 - Salaire
### Calcul du salaire brut

La société PSS désire calculer le salaire brut mensuel de ses employés en fonction d'un taux horaire variable et du nombre d'heures
travaillées.

<u>**Travail à faire**</u> : Compléter l'algorithme ci-dessous, sachant que l'on ne tient pas compte de la majoration liée aux heures supplémentaires.

```
Variables :
NOM : Chaîne (Nom du salarié)
TH : Réel (Taux horaire du salarié)
NBM : Réel (Nombre d'heures mensuelles)
SB : Réel (Salaire brut mensuel)

Début 
Saisir « Quel est le nom du salarié » , NOM




Afficher « le salaire brut mensuel est de : » , SB
Fin
```

### Calcul du salaire net

Par soucis de simplification on considérera que le taux moyen des cotisations salariales est de 20%. Le salaire net correspond au salaire brut moins les cotisations salariales. De plus les salariés bénéficient d'une prime de repas mensuelle de 80€, cette prime est exonérée de cotisations sociales.

<u>**Travail à faire**</u>: Modifier l'algorithme de la question 1, afin d'afficher le salaire net et le net à payer.

## Exercice 2 - Trophée Lancôme 

Tous les ans au mois d'octobre est organisé à Saint-Nom-La-Bretèche l'Open de golf « Trophée Lancôme ». Les distances des parcours sont indiquées en Yard (1 Yard = 0.91 mètres) afin de rendre plus compréhensif pour le spectateur les rudiments du golf, les organisateurs ont décidés d'utiliser un convertisseur Yard/Mètre. Pour cela il vous demande de réaliser un algorithme permettant d'afficher la distance d'un parcours en mètre en saisissant sa longueur en Yard.

<u>**Travail à faire**</u>

- Rechercher les variables et constantes nécessaires pour la réalisation de cet algorithme.

- Réaliser l'algorithme

## Exercice 3 - Convertir des Devises

Il est parfois encore difficile de retrouver rapidement la valeur d'un bien ou d'un service évalué en euro. Le petit algorithme ci-dessous pourra vous aider.

```
Variables :
PE : Réel (Prix en euros)
PM : Réel (Prix en dirhams)

Constante :
Taux ← 10.54

Début
Saisir « Quel est le prix en dirhams ? » , PM
PE ← PM x Taux
Afficher « La valeur en euros est de : » , PE
Fin
```

<u>**Travail à faire**</u>

- Réaliser la trace de l'algorithme, selon le tableau ci-dessous, avec un prix en euros de 300.00dhms.
- Modifier l'algorithme pour qu'il fonctionne correctement.
- Réaliser l'algorithme inverse. Cette fois on saisit le montant en dirhams et l'algorithme nous donne le montant en euros.

|Instruction|Nom variable|Valeur de la variable|Affichage à l'écran|
|:-----------:|:------------:|-----------------------|-------------------|
|Saisir        |P|||                                  
|||||
|||||