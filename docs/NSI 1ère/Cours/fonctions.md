# Chapitre 4 - Les fonctions

Nous avons déjà vu beaucoup de fonctions : `print()`, `type()`, `len()`, `range()`...

Ce sont des fonctions pré-définies ([built-in functions](https://docs.python.org/3/library/functions.html)).

Nous avons aussi la possibilité de créer nos propres fonctions !

## Intérêt des fonctions

Une fonction est une portion de code que l'on peut appeler au besoin (c'est une sorte de sous-programme).

L'utilisation des fonctions évite des redondances dans le code : on obtient ainsi des programmes plus courts et plus lisibles.

Par exemple, nous avons besoin de convertir à plusieurs reprises des degrés Celsius en degrés Fahrenheit :


``` python
print(100.0*9.0/5.0+32.0)
```

``` python
print(37.0*9.0/5.0+32.0)
```

``` python
print(233.0*9.0/5.0+32.0)
```

La même chose en utilisant une fonction :

``` python
def F(DegreCelsius):
	print(DegreCelsius*9.0/5.0+32.0)
```

``` python
F(100)
```

``` python
F(37)
```

``` python
x=233
F(x)
```

Rien ne vous oblige à définir des fonctions dans vos scripts, mais cela est si pratique qu'il serait improductif (et tellement triste) de s'en passer !

## L'instruction **def**

Syntaxe :

```python
    def NomDeLaFonction(parametre1,parametre2,parametre3,...):
        """ Documentation
        qu'on peut écrire
        sur plusieurs lignes """	# docstring entouré de 3 guillemets (ou apostrophes)

        bloc d'instructions		# attention à l'indentation

        return resultat 		# la fonction retourne le contenu de la variable resultat
```

**Exemple n°1**

``` python
def MaPremiereFonction():	# cette fonction n'a pas de paramètre
    """ Cette fonction affiche 'Bonjour' """
    print("Bonjour")
    return			# cette fonction ne retourne rien ('None')
				# l'instruction return est ici facultative
```

Une fois la fonction définie, nous pouvons l'appeler :

``` python
MaPremiereFonction()	# ne pas oublier les parenthèses ()
```

L'accès à la documentation se fait avec la fonction pré-définie `help()`:

``` python
help(MaPremiereFonction)	# affichage de la documentation
```

**Exemple n°2**

La fonction suivante simule le comportement d'un dé à 6 faces.

Pour cela, on utilise la fonction `randint()` du module random.

``` python
def TirageDe():
    """ Retourne un nombre entier aléatoire entre 1 et 6 """
    import random
    valeur = random.randint(1,6)
    return valeur
```

``` python
print(TirageDe())
```

``` python
print(TirageDe())
```

``` python
a=TirageDe()
print(a)
```

**Exemple n°3**

``` python
# définition des fonctions
def Info():
    """ Informations """
    print("Touche q pour quitter")
    print("Touche Enter pour continuer")

def TirageDe():
    """ Retourne un nombre entier aléatoire entre 1 et 6 """
    import random
    valeur = random.randint(1,6)
    return valeur
```

``` python
Info()
while True:
    choix = input()
    if choix == 'q':
        break
    print("Tirage :",TirageDe())
```

**Exemple n°4**

Une fonction avec deux paramètres :

``` python
# définition de fonction
def TirageDe2(valeurMin,valeurMax):
    """ Retourne un nombre entier aléatoire entre valeurMin et valeurMax """
    import random
    return random.randint(valeurMin,valeurMax)

# début du programme
for i in range(5):
    print(TirageDe2(1,10))	# appel de la fonction avec les arguments 1 et 10
```

**Exemple n°5**

Une fonction qui retourne une liste :

``` python
# script Fonction5.py

# définition de fonction
def TirageMultipleDe(NbTirage):
    """ Retourne une liste de nombres entiers aléatoires entre 1 et 6 """
    import random
    resultat = [random.randint(1,6) for i in range(NbTirage)]	# compréhension de listes (Cf. annexe)
    return resultat

# début du programme
print(TirageMultipleDe(10))
```

**Exemple n°6**

Une fonction qui affiche la parité d'un nombre entier. Il peut y avoir plusieurs instructions `return` dans une fonction.

L'instruction `return` provoque le retour immédiat de la fonction.

``` python
# définition de fonction
def Parite(nombre):
    """ Affiche la parité d'un nombre entier """
    if (nombre % 2) == 1:   # L'opérateur % donne le reste d'une division euclidienne
        print(nombre,"est impair")
        return
    if (nombre % 2) == 0:
        print(nombre,"est pair")
        return

# début du programme
Parite(13)
Parite(24)
```

## **Portée de variables : variables globales et locales**

La portée d\'une variable est l'endroit du programme où on peut accéder à la variable. Observons le script suivant :

``` python
a = 10		# variable globale au programme

def MaFonction():
    a = 20	# variable locale à la fonction
    print a
    return
```

``` python
print(a)		# nous sommmes dans l'espace global du programme
```

``` python
MaFonction()	# nous sommes dans l'espace local de la fonction
```

``` python
print(a)		# de retour dans l'espace global
```

La variable `a` de valeur 20 est créée dans la fonction : c'est une variable locale à la fonction.

Elle est détruite dès que l'on sort de la fonction.

L'instruction `global` rend une variable globale (Je sais, c'est fou !) :

``` python
a = 10		# variable globale

def MaFonction():
    global a	# la variable est maintenant globale
    a = 20
    print(a)
    return
```

``` python
print(a)
```

``` python
MaFonction()
```

``` python
print(a)
```

!!! attention "**Remarque**"
    il est préférable d'éviter l'utilisation de l'instruction global car c'est une source d'erreurs (on peut modifier le contenu d'une variable sans s'en rendre compte, surtout dans les gros programmes).

## **Annexe** : la compréhension de listes

La *compréhension de listes* est une structure syntaxique disponible dans un certain nombre de langages de programmation, dont Python.

C'est une manière de créer efficacement des listes. Revenons sur l'exemple vu dans le script de l'exemple 5 :

``` python
resultat=[random.randint(1,6) for i in range(10)]
print(resultat)
```

**Autre exemple : liste de carrés**

``` python
carres =[i*i for i in range(11)] 
print(carres)
```

La compréhension de listes évite donc d'écrire le code "classique" suivant :

``` python
carres=[]
for i in range(11):
  carres.append(i*i)
```

##Exercices

###Exercice 4.1 ☆

1.  Ecrire une fonction `carre()` qui retourne le carré d'un nombre :

    ```python
        print(carre(11.11111))
        '123.4567654321
    ```

1.  Avec une boucle `while` et la fonction `carre()`, écrire un script qui affiche le carré des nombres entiers de 1 à 100 :

    ```python
        1² = 1
        2² = 4
        3² = 9
        ...
        99² = 9801
        100² = 10000
        Fin du programme
    ```

###Exercice 4.2 ☆

Ecrire une fonction qui retourne l'aire de la surface d'un disque de rayon $R$ .

Exemple :

``` python
    print(AireDisque(2.5))
    19.6349540849
```

###Exercice 4.3 ★

1.  Ecrire une fonction qui retourne la factorielle d'un nombre entier $N$. 
    la fonction factorielle s'écrit avec un point d'exclamation, on note: `factorielle(n)=n!` Afin de calculer la valeur renvoyée par cette fonction on a la formule:
    <center>
    $N!=1 \times 2 \times ... \times (N-1) \times N$
    </center>

    Exemple :

    ``` python
    print(Factorielle(50))
    30414093201713378043612608166064768844377641568960512000000000000
    ```

1.  Comparez avec le résultat de la fonction `factorial()` du module `math`.

###Exercice 4.4 ★

1.  A l'aide de la fonction randint() du module random, écrire une fonction qui retourne un mot de passe de longueur N (chiffres,
    lettres minuscules ou majuscules). On donne :
    ```python
    chaine = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    ```
    On obtient:
    ```python
    print(Password(10))
    mHVeC5rs8P
    print(Password(6))
    PYthoN
    ```

1.  Reprendre la question 1. avec la fonction `choice()` du module random. Pour obtenir de l'aide sur cette fonction :
    ```python
    import random
    help(random.choice)
    ```
    

1.  Quel est le nombre de combinaisons possibles ?
2.  Quelle durée faut-il pour casser le mot de passe avec un logiciel capable de générer 1 million de combinaisons par seconde ? 
    
    [Lien utile](http://www.exhaustif.com/Generateur-de-mot-de-passe-en.html)

### Exercice 4.5 ★

Écrire une fonction qui retourne une carte (au hasard) d'un jeu de Poker à 52 cartes.

On utilisera la fonction `choice()` ou `randint()` du module `random`.
On donne :

``` python
ListeCarte = ['2s','2h','2d','2c','3s','3h','3d','3c','4s','4h','4d','4c','5s','5h','5d','5c', '6s','6h','6d','6c','7s','7h','7d','7c','8s','8h','8d','8c','9s','9h','9d','9c', 'Ts','Th','Td','Tc','Js','Jh','Jd','Jc','Qs','Qh','Qd','Qc','Ks','Kh','Kd','Kc','As','Ah','Ad','Ac']
print(TirageCarte())
7s
print(TirageCarte())
Kd
```

### Exercice 4.6 ★★

1.  Écrire une fonction qui retourne une liste de N cartes différentes d'un jeu de Poker à 52 cartes.

    Noter qu'une fonction peut appeler une fonction : on peut donc réutiliser la fonction `TirageCarte()` de l'exercice précédent. 
    
    Exemple :
    ``` python
    print(TirageNcarte(2))
    ['As', 'Ah']
    print(TirageNcarte(25))
    ['Jc', 'Jh', 'Tc', '2d', '3h', 'Qc', '8d', '7c', 'As', 'Td', '8h', '9c', 'Ad', 'Qh', 'Kc', '6s', '5h', 'Qd', 'Kh', '9h', '5d', 'Js', 'Ks', '5c', 'Th']
    ```

1.  Simplifier le script avec la fonction `shuffle()` ou `sample()` du module random.

    ``` python
    ```

###Exercice 4.7 ★

Ecrire une fonction qui retourne une grille de numéros du jeu Euro Millions. On utilisera la fonction `sample()` du module `random`.
``` python
print Euromillions()
[37, 23, 9, 11, 49, 2, 11]
print Euromillions()
[16, 32, 8, 30, 40, 6, 4]
```
<center>
![image.png](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAAFWCAYAAAD5UQZUAAAgAElEQVR4Aey9CZgU1bn/Xz//Se6S5Cb35sZcb/Zc401MYtQkGjUoIgoGYtzFBdFgRA2CikpAWQTcURBF9h1Zh2WGbdgZBoYZYJh93/e9e2Z6erpnevn+n1PnVE91d9VbVU3L9Qk1z3Omq89b9X3Pec+nT1VXdb0lSaq/Kx79Gz548xYUrJKQv+YSuRSs/RIK1v4TCtb9KwrXfx2F67+Jws++haLPvo2iDd9B0YbLULzxuyje9H2UbPoBSjb/CCWbf4LSLf+D0q0/RenWK1CW8DOUJVyJ0m2/QPn2X6J8+1Uo2/FrVOy4BuU7r5VLReJvUJH0O1QkXYfKXdejctcNqNx9I6r23ISqPX9A1b6bUb33FlQnD0b1/ltRs38Iqg8MRfWB21F78A7UHhyGukPDUXv4j6g9PAJ1R0ai7sifUHf0z6g/djfqj92D+pR70ZByHxqO34+G1AfQmPogGk+MQuOJh9F08hE0pT2KprTH0HxqNJpPPY6W9DFozngSzRl/QcvpsWg5/RRazvwVrWefRuvZcWjNfAZtmc+i7dzf0JY1Hu1Zz6M9ewLasyeiI+cFdOS+iI7cl+DImwRH3stw5r8CR8FkOAr+DmfhFDgLp8JZ9Bo6i19HZ/E0dJZMR1fJDHSVzkRX2RvoLpuF7vLZcJXPgaviLbgq30ZP5TvoqXoXPVXvwVX9PtzVc9FT8yF6auehp3Y+3HUfwV23AO76hXDXf4rehkXobViM3sal6G1cht6m5fA0rYCneSU8LavhaVkDb+taeFvXwdO2Ht62jehr34S+9s3wdmyBtyMBXsc29Dm2o8+5A32dO9HXmYT+rl3o79qN/u496O/eC58rGT7Xfvh6DsDXcwg+9xH43Ufh7z0Gf28KfJ7j8HtPIOA9CX9fGvx9pxDoz0Cg/zQCvjMI+M4i6M9ERX0axi38DCoswxfHTroH+QxQVlZLNqw2rP9nsAZ9WThTejwa1tvHPY685RLyVwhQY4C1eKM9s9oza/xmVk1YUxf9kwyqDat9GPBFOgxgsD7w7mo+sw4dNxq5yyS5MFBjhbU8cRCaz72F1vyFaM9fiLaCT0VZhLaCRWgrXIx2uSxBe5FSlqKjaCk6ipeJsgIdJayshEMuq+AoZWU1L2Vr4JTLWjjLWVknynp0VrDyGS+VG9Apl43oqmJlkyib0Vm9Gd3VW3ip2YpuuSSguyYBrtptomxHdx0rO+Tiqt8JXhLhqk+EqyFJLj0Nu9DTyMpuXpr2wC2XfXA37YO7OVmU/ehtYeUAL60H0SuXQ/C0HoKn7bAoR+BpPwJv+1FeOo7BK5cUeDtS0Oc4Lkoq+pysnJBLf+dJ8JKG/s409HWdQl9Xulz6uzPAy2n4us+g38XKWVEy0d9zDj65ZMHXkw2fm5UcXnrz4OvNl4u/twB+DyuF8HuKePEWwy+XEgS8pQj0sVImF7/nnMExa6qpY9azJeIQYPrrtyJ3KQeVAWsV1pItP4ezcjvsPzsCUREIeOD3sA+A3hcsc7DK36aWvPVrDmoMsBZvugIeR1FU++wKOwLqCAS8eecFqwD1KuQskWKCtac5Xd0ee9mOgG4E/L1nYoaVgzrnVzKoVmGt3P+AbqNsgx2ByAgE/R0xH7MOgLpYsgxre9HKyLbY7+0IkBHwuVNiglWA+ktkL5KQYxHWnuZTZKNsox2ByAj43BkmrmBFf8ESoF4lg2oVVhvUyGGw3xtFwOdON3m5NRxWDupbVyF7sWQZ1n7HcaN22XY7AmER8LlTxW8D9lmaWQWov5aPT63AWr1FAtyHwxphv7EjYBQBX/du1Q9ZzMMqg7r0rWtDX6TMwtq8SwJ6jxq1y7bbEQiLQL9zY8SvrszBykF951r5HCo7PcWKGVgdB9mMau/6w0bBfmMYgX7nOvQ5EyzDKoO67N3f8mv9SwdOURnB6k1jM+oJw4bZK9gRUEeg37kafY71Gr9npWdWAerv5Ov78g9TTMIazGagpqnbMLAsSUC8y4B6+FK8/TA9vT/bl/lx1YlhX8dyeNuW6/z4Wh9WAep1yFvJf4xiFlaUMFB1Lp9Sg63TAbKa0qNspKiOkdKjbDpyZDWlR9lIUR0jpUfZdOTIakKvv30J+loXEHcKaMMqg7r8ves5qBZgRakEeE5rt5doqPYGBrWUHmUzkNU0U3qUTVPMoJLSo2wGsppmSo+yaYoZVBJ63rZP4Gl53+C2lmhY+Yz6/o3y7SfyrGoC1orNElB+GeA5q91ioqHaGxjUUnqUzUBW00zpUTZNMYNKSo+yGchqmik9yqYpZlBJ6PW1fgRPwzQT92CFw8pBnXsj8ldfYhrWSgZq5ZWA95x2i4mGam9gUEvpUTYDWU0zpUfZNMUMKik9ymYgq2mm9CibpphBJaHX1/whPPV/N3nD4ACsAtSbkL/my6ZhrUv6/4Cq3wPebO0WEw3V3sCgltKjbAaymmZKj7JpihlUUnqUzUBW00zpUTZNMYNKQs/b9D481X+zcHcrh5Ufo84dhPy1/2wa1pbDPwBqbgO8udotJhoa2qBjl/gGuQgIhGq1Fyg9yhbsAGbfpvqmOhg40qHtQ6ml9Cgb8/XmUJWvCUB1v6Kq/UrpUbaQmgeYdQn3uc4ZqtVcoPQoG3qBRyPP4gwGOjW98EpCz9P4FnorRlu8u3Ufv2dq+QeDULD266ZhbT3yv0DdXehtjeVbvxtImqYa0M8R1JT7hZ/ngLefEsv3A92xBVk+5aa36Wmh/7NpwHQF2AQgqLcBeHv0zMRg802CQOq4gTh+TqAGA2XCx7XApNeAV18DZm0D+vQaTvfL0zgb7vKHLOcN4DPqh7egYN2/m4a148RvgfqH4WrUOeFPBbl1Ge/4oLtEAD4vUAPAudXAi7OAUi+CcAKXspnhe0AzQQ/VdsrmbQdyc4FuD5Axh/ftyoPEaNIDSn4omGrLFhE/Mdt9TqDCuTnczzeeBVLbY+6Xp34m3GX3ordluSVYZVBXzBuMgvWXmobVnf8noHEsXI0ntRtMDmg9UO4Ceg99zqCqmxYA0iYJf8sAv9oWsUy1nbIpMjuvEn7uB5oNjmkoPcrmU2a5l4Ad4tDm8wK1fD7vz2UvAEumiL7dDDQRfSPa3ls3DT0lf4Kncb6ljCwC1CEo/Ow7pmF1FzwIND8fG6jKgF4wUP3A3mdEgEcA1T6lBdqvRJANZzm2n2/KARLfEf7Gx/0wIwg38IqYRZMrgI3Xc1/zigB3bPAY90sJlQ94R/je51Iqo1+JGPbWToG7aDg8DR9aSh8kg7pyHgP1e6ZhdReNBlpehasphhlV6dYFAdUHbLpXQPMM0GAAKWsbEWR9WwAo2AXMWwk0+xFEDzBcDGgh8YUqBl9Bf4noj9BnGkqZXa1EN/o1Bl/yAXbBOmDCZCCnF0A/MFP4O9QT7UOpIXy5a15FT8Ft8NTPtZDrKpF/mVo5fyiKNnzfNKzBxqlA20y4mohr/Uqj9V4vBKiFMwYG8elpwNQZwIxVQGe8j1GDwJ47hK+JwMxHxPJYgJh49MGnPjBu4GgSkLCDlylf5b7GbgAqGUw6fwQ8ZDuyJ4q+jAY+HD/Qrxi/kPZWTYIr/2a4qyZZSszGZ9SPbkfRxh+ahhUd74KVLzKoQUaIMqspM47yWk4cpMY6oFGnwkYDOcSsw3iK1ZeaxcPiA/J5HaOyQ40lowSgbDYdA5R61C2IXib61Vs1Ea68G+CunGgpi6AA9Q4UbfqJaVjhWABf3ezzAzW6e/o1RMfJwdZX1LfYvvRjY9ZCxNBdMR6unN/AXfG8pZSXHNQFw1G86XJjWNfwiwJwLkVv2VS4mnTuQiUaaravYetRepQtTMTkG0qPspmUD1uN0qNsYSIm31B6lM2kfNhqhF5P+bPoyv4V3BXPWcrPKoO66uPhKN78v6ZgLd36M6BrPdwVs6B7FyrR0LAOmX1D6VE2s/rq9Sg9yqbWMLtM6VE2s/rq9Sg9yqbWMLtM6PWU/RXd565AT+E98DSbTyYsQL0TLNGZGVir994KdCfAXfkudNP5EA0129ew9Sg9yhYmYvINpUfZTMqHrUbpUbYwEZNvKD3KZlI+bDVCr6d0LLrP/hjd+XfwzNcmYZVBXf3xSJRsvdIUrNX7bgd69sJdPZ8GlTU2niUsEqo38fShaKnkwxYVezxfwxyo3sTTh6Klkg9bVOzxfA1zMPDGVfIEus5chu7cIQNp2k3AykH9ZCRKt/7KFKzVyXcCvUfgrlmsD+pAu+wlOwJhEXAVP4au0/+G7pybw58poAOrt+0z+QoW3/UvvAulCVeZgrVm/0jAkw533Sr0NGeENcJ+Y0fAKALdRQ+jM/0SdGXfGP0ADAJWPqN++meUbLvGFKzNZybJv0PtrlqGnhYbVKOBse3hEXAVPAjnKUkumk9r0YE1BGrp9mtNwdqWMwvoL0Fr9kwb1PAxsN+ZiIAr/150npDQeVLSf7SQBqwC1HtQuuO3MANrW87bQH8N2rLfRE+Lzs19Jhpsr3JxRqA7/89wpkpyIZ+DFQErB3XRvSjbcZ0pWNtyPwD8rWjLfc8G9eJk7bx63Z07Eo7jEpzHJbjrPqYf2qaClYO6+D6U7fy9KVjb8xcAQRda8+bDrTejxvM0h6KlFx7FHs9X29f5n1rUiWF3zp1wHJPgSGGgsicMmoNVBnXNkvtRlnijKVjZY3jYL4/ZI3rcrfbt0jrjYa6afbj0/uL5wVO0KF96tljqiX65su+A84gE51EJPVUzTcMqQH0A5Ul/MAWrDGcwgNb8RTaosQyiehtiQON6seQLBGp31lA4D0tycZU+r3p2Kz2zyqCuXfogypMGmYKVnZIKBvrRnPWeDaoauliWL0ZQM4fAcUiC85CE7pLxEQ8a1odVgPoQKnbdYgpW9kOUgM+D2qN/gbs1U3t4qAHQ3oKupfQoG62qbaX0KJu2Gl1L6TFbvItea6h26G1D1RN6XWeHoOOgJBdX8d80noqtDSsHddnDqNh9qylYXU0nEOh3ocYGlRoqczZiQOMOqZEvcy02txbhq+vMEHTsl9BxQEJX8XPoqdF6hHs0rDKo6xioe24zBWtPQwr8XiefUdtiSekTALIXqmaLu4HDsd9+Kw+oZvi8wEs6s1KWV3MLuZIIsr4vAME2YIq40Y5pSHcBmdR9KCZ+4S/r6PTh6W2AIwD0NwGrnlDFU2f9WPqlmSSkD9g0VuXvZaAx4o4Jwlfn6dvQniyhfb+ErqJn4ap+3xSsHNTlj6Byz1BTsLoaj8Lf2xo7qC7lNunLgNl/Fx0eFfe7NeV7oo8v5AkTWNKE50cKX08BznjfMwUgU9zp+uUZwNIXhK+58v1wup8KYkDJGfW7iVxy+QvA9AN8eekVKnh0YNVrSFQ7iCQhLeuEn4eB6cP58lXJ4Yk2ovQGHDtPD0HbXgkdeyV0FzyDnqr3TMHKd/0rRqNy7+2mYO1tZ08ebogd1L038M6NYse3AaAwDciu/nzvtWf3T00Qg3fWPRA1rSUiyOSMWvw679eYg0DxJjGYq+h0RUa+mF2rzGuHH2cHbNlAsC8iIYXWdlr9ZXWR7aCShCRcztdf3IqBLCojwieaSD2VX2fGELTukeTiPD0UPVXvmoI1BGrVvuGmYO13VYMV+ctUW5aqCapF3Yb6gA9E8K++UwT6cWBfk2pjjUVdPY0ga2yOqrnc17gcLWt4XYy+5EwsY9VgfRPIif0mOE1AWdtYuTsT8s2Lw9j7RwAv+5yfFvFUtyFiObynA+8i++zVSxLSD8wSmju7EESb8Pk1oEm1l4rUG/AER/oQtOyW0LZbguP0beipfMcUrHzXv+JxVO6/E2Zg7esqBysM1N5YQFUSGEjPArvXiI7+Aqj7fJInyPfYP8SC+3OgkfChBJMIctTMo2zDXjOe5n359lRgiXJIMwHsJk7dPyNfzK5XPha3qrdmAa0M1HT9dRUNvYbotSPqlvZ+4A01qK3Cp3lQO9KGoCVJQusuCY6M2+CqfNsUrDKo61c+gcr9fzQFq9dZBFbYbwN6263OqAASRcqbGZU8ocEc0fFjxIjqBZIFnrIxuzdVBHND+HGU1UEjffUBk0U/0nt5AooR4n0xkfSCajuzmS1lAHrXG69vtc9RoAaBTT/gfhazT0ep8DnS9K7fcXIIWnZKaEmU5NnVVfGWKVj5jLrqSVQd+JMpWD0duWCF/TaAHa9q/lED4EgUnbseeHumWL4aaCBmO0qPsrHGFU7mPqZVaDY1qpLSo2zJw7ifr7wCLFC+TD0QPoCRzig9ZtMtG7jS0meAV/fx5ff/hVhfaEX6V97rtSMKVACNy4Wfx4CZI/jyNfvDJwE9PQDtJ4egaaeE5p0SOk4NhqtiDszAymfU1X9B1cG7TMHqL5+O3raz8uXWmEBlX6BOvKkK6tXA9kYlZNqvRMcNZ9RDIolYQpe2dmRtrL5YAoqZQ1T9GgqkOCLVw98b+WJ2vfJGKv8C6q4G5iuZEYn1jXyFt4y/0wKVHRBvVaW7lKydnuo4MQRNOyQZ1va0W9FdPtsUrALUsag+dLcpWP3FT8DdfEq+3BobqFoRMaiLJcgGkrrmL5IvPUhjrdfrNNVnvW2oekKvLXUIGrdJaNwuoe3kYHSXzTIFqwD1KVQfuscUrL6yx+BuPC5fbu1t1/kWTTSU6p+ujdKjbLqChIHSo2yEpK6J0mO2eBe9hlDt0NuGqif02o7fioYESYa17cTN6Cp7wxSsMqifrfkrqg/fGw1r8kj5uLVq352oSB6O6sMPor/iUfTUH5GvYvV22KBS42VoIwY07pAa+TJsrIUVCF9tKbeifquE+gQJzcn/iq7SmaZg5aCufRo1Rx4QsCrAshn2HlQdZIcEf0bVgbtQd/I59FU9jp76A6jcOxyejjzt1hMN1d7AoJbSo2wGsppmSo+yaYoZVFJ6zBbvotccqh1621D1hF7rsVtRv0WSS9O+r6KrdIYpWAWo41Bz9CHUHn1Ifq05onply4ceAqtrSH8R/bVPwlm2QQbY48jXbi7RUO0NDGopPcpmIKtppvQom6aYQSWlx2zxLnrNodqhtw1VT+i1HL0VtZsk1G2W0LTnq+gsmW4KVhnUjeufQ13KaNSmjJZf61IeR23K46g7Nga1KbzUHX8SzWdfRF/Tc3CUrkXN4YfhcRZqNzfeASY6HvfBtH3F5wOiTQZajg5G3QZJhrVx99fQWTzNFKwyqJs3voD6E39B/YmxqD/xlChPo/6EUsahPm082nLGwdf6hDyj1qWMhddZrNMcu9qOgHYEmo8MRs1GSS4Nu/4FncWvm4JVBjVhy8uoT3sW9WnPofHk32QoG0+Ol1/rTz6PhlMT0HD6VXQUvgafcyY6yzeh7uTf4O0s1W6NXWtHQCcCzYcGo2a9hNrPJDQk/TOcRa+ZglUGdfu2KWhMn4CGjBfQmPGSXBoyJqExYxL468toypwOR+lc+LrXoLNiCxrSX0JfF7t2Z//ZETAfgaaDg1GzTpJLfeJX4CycagpWGdSkxOloPPMKms68iqYzk0OlUSw3npmC5nOz0Vm5HD73MXRWbkVjxmT0dZebb6G9ph0BAE0HbkHNWkkuDTsYqFNMwSqDumvXLDRnTkVz5mtRpSnzdTSfm46W7LfhrFoOnysJnZUJaDozDX3dJq+f20NkR0BEoHH/LahaI6F6jYS6HZfAUfB3U7DKoO7d/TZasmaIMlN+bc6ageZzrExHc9YbaM2bh67qJfB1foLOyi1ozpqFvm72Cyj7z46A+Qg0Jd+CqlUSqlZLqN0qwVEw2RSsMqjJe99DS/asUGmSl2ejOedNNOe+jda8D9BRsgI9Davhc7yJruqdMrh9PTXaLbRPT5k/xaMdQfPbW4n1F8BX475bULlSQuUqXpz5r5iCVQZ1/4EP0JL7pgwme23Je1eGszV/PtoKP0Z78VJ0ViXA07oBfscb6Gk4jI6SlfC567W7biV4ZtfV9sQHVM8WSz1rj94fZdPbhqqn9Cgbpalno/Qom54eVU/oNey7BRUrJFQwWFdKcOS9DDOwyqAeODAPLXlvoy33XbQWfIi2ogVoL1qEjpKlcJSuRmflZnQ3HkGfYxWCna/IP/NzVu9Av6dZu7msofEu2p5sUPXiEllPwCOPVeT65/Oe8FW/dzDKl0soZ7CuYKBO0oa1KPxsgAzqwYMfoa3gAw5o8SJ0lK6Ao2wNnBUb4Kzehu6Gg/KPpH1diQg6Z8HbVQZX83H4+jq0uxNvSImOX8gg2760hzuqlhiv+j23oGyZhHJWlktw5L5kClYZ1EOHP0V70Sd8Bi1fLQBNQFfdbriajqC3/Ry8rmr4e44DnYvR39sEj7MA/j6d5wzaoEaNnWYFMaD/qB+Kut2DUbpUkkvZUgkduS+aglUG9dix5egoXQZn+To4qzajs3Ynuhv2o6clFZ6OHHhdlfD3tsPvPoOgcz183k70uesR8Ok8e9MGVZPLqMqLEdRdg1G2WELpEl46cl4wBasM6omT6+Go/Axd1Qnoqt8DV9NRuNsz5FlTvj3a2yrPnkF3FoKOLfD398DndcrJ0qKCzyp0Qf0VwL5/nb4vYh29etWxrqYj4uY+rSwfUc8rHQwciTh8iQUePV9vDlX1cwJQHfGk6Xj5CsXGA8y6hPuMfDZq3Hz1Ao+qxkUe68FAZ6gR3L/qrXqxNulmFC+WUCJKe/ZEmIFVBjUzKwlddQzQw3C3pMmzKLs8ymZNn7ddhpTNnoHePAQcifJMymANBiNSuSgtkhsf2ZmJQIlYIQxUvfqI7RXtyNeoASCyfKTcL8B5Dnj7KbF8f/gNeFF6KodRNsLXaaH/s2nAdAXYBNM3wUXv+glfchODQKrqXqbzAlXf10DSiWuBSa/xTDSztgF9VJwGbDKoiyQUL5JQ+qmE9uwJMAOrDGpZeTp62zPhcebLl0UZoP3yLNopz54ypH4Pgu4iBDr2IuD3yLAGgzp3jkaBehfgdQLLlvIOhUDVq4+ANAqQgY5HDahulo8AcG418OIsoNQLOWHEpczP94Bmc8kTzPtit2m3A7m5QLcHyJjDPxRXHlQ1nNgbsLUi+6zbLyHZEpEp5XxApXw5N4sPuBijbzwLpEbkDotsu6rXNYm3oPgTCcULJRQtktCe9bwpWGVQ+12L4O/ZAJ87BT5vLfx9DFAXAj43Av5eDiYDtScLwY59CPicch1Mg6qAdxnAbswMgWpUr9gtnNvUzfKhiha7EzZtkgj4svB0QkSQo+Ax42unyGMg3Q80R3yw4+XLVyb68hKwQ9x1ez6gUv0qn899XfYCsGSK8Hsz0KTqG9Gvmp23oPhjiZdPJLRljTcFqwxqc9NR+NxbEexZhKB7LYI9uxDwnEOgvxFBv1cuAV8ngs6DfEZ1531xQVV41LzVlxn9wF6R0EwaAVRHJIggghwFqqGvINCUAyS+IwZ0/HkcZghnEf0KslQsr4gPdHIFsFFkFJxXBLjNwWO9X0rHfYCS+WafKnMhEcPqHbegaIGEIgFr27m/mYJVBrW04qy8y/d5GhDwZiPo3o+gKwkB1z4Euw4h6EpH0HkIQcc+DqojGYGuEwj6rJ6eugAzqhLDiAHl1T5g070CmmeAhghI2UpEkHVtUb4CQMEuYN5KoNnPM6cMFzAVqr5QxcFX0F8i+qPa+zBdVmZXK9GIU7+CQME6YMJkIIed8ekHZgpfh3pM+arefguKPpJ4WSChLfNZmIFVBjWv6Bg8jmz0uWvh63PIu/wg27178hHsTkGwMxlBZ3IIVDBQ2Xt/xDdmpalKoKJe/49BLZwxMKhPTwOmzgBmrAI6YzxGVfobBWoQ2HOH8DURmPmIWB4Lllgw9Mfio/enZ4vy5QaOJgEJO3iZ8lXua+wGoFJ1+lBPj/nXs0X5ApA9UfRlNPDh+IF+qecsPT0AVQm3oGC+hEJRWjP+YgpWGdTcvEPobU9HX1epfDKfnchnX6BCu333WRnWgGMvAh17EHTu4/AGVIFQB5w1VLP834EqZ79TZrXItpWrzl4QQbY0oFGnwkYDOapZhwKEsmnBo479YfEBOZ9jVEVP05cbWDJKNb5jgNKIrIVEDGVQ50khWBuPjERr5jOGsMqgZucdgLv1BLzOfPT38FnV73MjyL5AsWPUvnoEu9ksuhcBxx4OaU86rH+Z0gPYRL0SvMhXIiiRq5p6T+lRNlPiEStRepQtQsbUW0qPspkSj1iJ0KtIuBn5H0q8zJPQcGgkWs+OM4Q1BGpPSwo8ziz5WNXvaZG/9SugBjzZCLjYceleBLr28GVXMmB5RjUBJOukVomIRegtEZTQOlYWKD3KZsWHsi6lR9mU7a28UnqUzYoPZV1Cr3LLzcj7QEK+KPWHR6D17NOGsMqgZuUmo6f5KHo7zqCvqwT9vY3w97PdvwdBXzOC7mQEe1MQ6G9HgJ3o9+Yj2MNAtXqMqgMh65hRUYIQ+UoEJXJVU+8pPcpmSjxiJUqPskXImHpL6VE2U+IRKxF6FZtvRu5cSS55cyXUH/wjWs781RBWGdRzOXvhamLHqafg7SyUf2fq7+tCwN+NoPcYgr3JCPra+XlVBio7HPCctUGNGB/Lb4kB1T0etuxEbPAF8VWx6Wbkvich931e6g8MR8uZpwxhlUE9nbUbrsb9cLemwuvMg89dx0/692Uj4NmDoK9KPl5lj+2RAWaXU30uIKCTfJcFJd5Fb4Di7cdoQOPt7yLrV/mmQch+V0LOe7zU7R+GltNjDWGVQc04uxPdjXvR03IMXkeOALUGAc9e+Psy5ZlU/iGKp0N+Igq7asWK7rV+veDb9Rd9BMo3DkL221IIVgZqc8ZfDGGVQT15ehu66xPR03xY/kLV765GsO8kgn2n4O/vkH8g3e9pQX93Bfo6S+DzNMPnZU/FUJ28vuiHwA6AmQiUbRiErLckZL3DS23yHWjOeNIQVhnUE+mb0VW3HT3Nh2RQ/X3FCPadg89TJ8+u/ZGhSDQAACAASURBVK4qeLvL0NtyCr3s11VdpfLZAfbjFPvPjoCVCJR9JkBlsL4loXbf7WhJH2MIqwxqStoGdNVtk2dUb1caAr6z6HeXyl+svM5c+aqVpyMLPfV70VO3G56Oc/C0s98CqC+zWGmuve7FGoHS9YOQNUfCuTclZL/JQB2K5lOPG8Iqg3rs5GcyqO6WA/D3LEe/6wB6OzLkL1fs/KpSuirWgRX5ffNR+PudF2u87X7HGAEG6rnZErJYmSOhZu8QNJ8abQirDOrxtI3ort+Jfsfr8DnfQU/LEbiaktHdsEeUfehuTIajaIFcuhv2obt+j/yj6hjba292kUagZN0gZM6SZFgZsNV7bkNz2mOGsMqgpmYkwNM8F8HWwXA17kB33Q501W5FZ+1WdNWym/y2y8WRPwesdNVuQ2fNVvi8LdrhjvcpHPuUUXxO92mPVny0I8dcx1fJ2kHIfEOSYWXAVu++FU1pjxrCKoOadmYz0PALuBuXyYB2Va2Hs3ot2GtX1QZ0VrOyEe1ZL6Mt6xU4qz5DZ+U6+Dw6j4akwNLpAFlN6VE2UlTHSOlRNh05sprSo2ykqI6R0qNsOnJkNaFXvGYQzs6UOKxvMFAHo+nkI4awyqCWZg6Hp+5VdNVtRWf1enRVrkJn5QpeKlahs2K1XOQfD5wdB2f5CjjKl8mXWjUbTDRUc32jSkqPshnpatkpPcqmpWVUR+lRNiNdLTulR9m0tIzqCD0G6pkZEs7MlGRgq3bdgsaTDxvCKoPaV3Inumo3o7t6PZxVDEoVqOXL4axYJpfWjNFoyxwPR9liOEo+RX9vg3aTiYZqb2BQS+lRNgNZTTOlR9k0xQwqKT3KZiCraab0KJummEEloVe0ehBOT5dwhpUZEqqTBqHxxChDWGVQMzKWorN6DbqqVqKragWcVWw2XS4XZ8VydMqgLkVbxmNoz3oBjrJFNqgGY2XKTAzoP+q1/sJVg5AxTZJhZcBWJt2MxtQHDWGVQd1/ZDmcVctCpbNymQB1WQhSZ/kStGWMkm/EcpR9CkfpQvT3EknSTI2UyZUuwgH9hwV15SBkvC7J5fQ0CVWJf0BD6gOGsMqg7j68GM5KVpaEl4olcCqlfDHaMu5He9ZzcJQthKP0E+ugaiVqYHeEZi9UffO8Gzhs/vZb3QGN8uUFXtL5sUyWd+ATE8uHIsoXgGAbMEXcaMc0pbuAzIgLJPHyFWgCJl83EMNr3gc6VDf2sd5Z8RV1d4I6UUcfsGnsgC+Lz0ItWDEIGVM5qAzYqp03oeH4/YawyqAmHVyIzopP0VmxSC7OikVghb13lrPyqVza0u9C+7mn4Sj9GB0lH6G/t25ggNVLUUHRT2gA1yHR6cuA2cpz7kedx92aer78wPGFPGHCq68Bz48Ufp8CnLHeM6XnC0CmuNP1yzOApcrTpufK98OFQhUVp5BFAyw9X35g3TV8/RteB8b8kC9fkxx7sgsqUUfLOhG3h4Hpw/nyVeZ9MVDTp0gc1qkSKnfciIbj9xnCKoOaeOATdFYwWBfCIYqzfCGU0lH+CRxlH6MtfRjazj0BR8l8dBR/YB5UKqHB3ht4Z0dlAmx2LUwDsqtjv9ee8hXiwAVMELPr2YifKlqBh/JV/Drv15iDQPEmMbir5C6GmhEHX0H0AHeIvpT5EfQXCV83mk6zEz7bGiTqSLic6y9mP0pS8gmMMD2x5C8fhFN/l+TCgC1c8X3Up9xrCKsM6o79C+Co+BjO8ohStgAOuXwER+lHaE37PdoyH0VH8Vx0FL2HfndtKOZhC5EDoJvQwAd8IIJ89Z0iwI8D+yLOz0bqqZ1F2nR9qTaqmst9jdN4lmuknmqz8AFl2VDqgXIXoHETnJyJZazoG9OUvgnkRPyIJy6++oE5ws9HRUCBSBDBtCvP86bFqEQd/cAs4WtnF4JoE2P2NaDJ3F6pYOlNODWZg8qALVzxXdQfu8cQVgHqx+iUQV0AZzkrH8FZxsp8OEpZmQdHyQdoPfZltGc9JUPaXvQO+t1EanT1ACvLUQOqSmAgPQvsXiM6/gugTnWMZWVAdX1xgzwDPcSC/XOgUeVD2S5evjKe5n359lRgiXJIMwEsX0ToL16+3OeAYQIgpqmUalX/LPvSStTRD7yhBrXVMqh5S2/CyVclpLEyWULB8u+h/tjdhrCGQHVWst0/m1EXyIA6yz7kgJZ+ID9fqqPkfbQek9CWOwEM0vbCt+IAKoDEq3hnZ7AHV6hmh2OqEbUcZGjOcjIg3lQR3A3hx3AKPXHx1QdMFgOa3ssTUIwQ74tVSS/i4osdMXmApnqgox1wnxX9uw3oUjpl8csU9BJ1BIFNP+D6i1sBf6nwNdL8rn/JTTj5iiQXBmvhsv9G3dE/G8Iqg7rzwCdwVi6ENqwfwFEyFx0l76HtmISOnPFoL3ob7YVvxgdUR6Lo7PXA2zPF8tVAw/nMBgSohZO5j2k6jx6KFzzJw7ifr7wCLFC+TD1gekCjDjMU5qL2SkFg92Du66Y5wLif8uXhx5Ut+KuVflGJOhqXc33pMWDmCL58zf7wDz3hK2/xH3DiZQknXuGlYCkD9U+GsApQP0Vn1aeGsLanSmjPfR4dbEYtejs+oLLjoBNvis6zWedqYHtj7EFWtowaUGE4JJKIJainG2UjqzOP2E7LFzvFM3OIql9DgRSWIU71RwyoeVAB+GqBR7804Gv4MqBLdczIXJr0ZZyowwtsVaW3tHh6Km/RH5A6SeKwviwhf8llqDsy0hBWDupBBupiQ1jbT0poz5uAjuJ3ZVgtH6OqxsjSoskgW9LUW9n2pRcZ8/VEDHMZqC9JMqwM2Pwll6L28AhDWGVQEw8tQmf1EkNYO9IkdORPlA8DGKymv/Wb76L2mkTHyZlCW42utX3R8TFjJWKY++lNOPGiJBcGbP5iBuofDWEVoC5GVw273h8Oa2dF+BcsR7qEjoIXQses/b0mT0+Z6Ry1DtFxG1QqcCrbFySGOQtvQsoLElJfkHD8RQl5i/4TdYeGG8LKQT24BF01y4xhPS3BUTQFDnYmoGQubFBVIMSy+AWB50J+2HM+uQnHJ0ohWPMW/idqDg0zhFUGNenwUnTVrjCG9aykOnVl4cpULIOo3uYiHNALCc+F9JX9yU04NlGSYWXA5n7yLdQevMMQVg7qoWXorltpDGumJC4GzAc7z0pe62dwxbOowVUvx9OHoqXWVy8r9ni+qvXVy/H0oWip9dXLij2er2p91XL2xzfh2HgJKRN4yVn4TdQeuN0QVjGjLkd33SpDWLuyJHFRgF+18un9KEXVMHvRjoA6Atmf/CEEKZtZGajVB4YawsqPUY8uh6t+tSGs3TkMVOUL1kewQVUPgb1sJgIKqAzSYy9wUGv2DzGElc+oR1fA1bDGEFZXriRfFFBg9Xl0fjhtpsX2OhdlBLI/GSQfo7Jv/qywGbVm/60wglUGddeRVXA1rDOEtadACl0UYLDaoF6UrJ1Xp7MX3iLPpGw2VUCtTh5sCCsH9egq9DR+Zgirq4iBOnAFywb1vMbsotxYAVWeUV8Ux6h7b4ERrALU1ehp2mAIa0/Zb8IuCrDH/dh/dgSsRCDn01uQ8qIkl+MvqUA1gFUGdfexNehp2mgIq7v82rCLArqgxvM0h6KlFw3FHs9X29f5n1rUiWHOp4M5qC9JSBGgVu35A6oZqASsAtS1cDdvMoS1p/LGsIsCJKg6DY2pmkGo90fZ9Lah6im9eH4YFC29tlDt0NuGqqf0KBulqWcj9HIWDZYBPT5JAis5C/8NDFQjWPmuP4WButkQVnf1UHTVLg/B6vNG/BxPaTjRUGUVS6+UHmWz5ESsTOkxW7yLXhupduhtQ9VTepSN0tSzEXo5i25FioBUAbVy900C1pt1Z1YZ1D0p69HTssUQ1t7au8VFAQ6rDWocwI1hsPU2IesJeC7kJVQGqjKbHn+Zz6iVu2+EEawC1A3obU0whLW3/mHVRYHlsEG1QdX8cBAfiuzFQzioL0tIVUDddQOMYOXHqMcZqNsMYe1tHBt2UcA0qGRCA9FVrSQOShSIjkfNBpQvymbWF2uLuoxaBzSz+6B8QN6GcNsDa4F6L+RfzSdPD7cpGorfyNfIPptpOzzArEu4n/N5xCTpqxd4NCIG0mDTt2bnMlBfluSS+gqfUSuSrkelAawc1NSNcLdtN4TV3Tgj7KKA3xtxW7MS7MggUwkN2G2ZSdNUg7go/N53phmpp/jRslG+KJuiaeSL2UPlU77VgTeAB9/m90N1fCrs73Db+onAmLV8OVHkMAhtb+FLomHbg0Cq6haR8wGV8DVwL/+1wKTXeEKPWduAPiWA9HgxUBmgSsn5+F9RkXQdSFiTB0OAugm97TsMYXU3vxl2UcAcqAYJDagkDkrfjeBR1mP3X51bDbw4CyhlM5kTuJSB9T35ceT6NtX9RUa+1JBJw4CJzw2Au8ULPw4OvA+t+wxPqLH8R9G2UNsjFsLaQfVLtL1lS7h2zKAa+HJuDvfzjWeBVPMpmHKW3IbjCqivSsj+5KuoSPydIawc1BOb0du+0xDW3pa5YRcF/H0mZ9TQGASAtEmio8v44BFJHEKbhQ1aqJYv6No0fIU2JWy6emKmYHbNMoXfibn3zxH2XdxrkzLTRmwfalPEgm47NNruUzKWvATsEDcvxgyquh0avspFgovLXgCWTBF9vRloMnfXcN6S2/hs+qqEVAFqeeJvDGGVQd3DQO1INITV07Yg7KKAv69Z3auBZc0gayU0GNhEK9tIyKqpJ6yaNsoXZaN3W9qAMvBm8w9dw4oISBUoxwIsTVeRcju4Um9h1y93N7rtQXbo9IrQS64ANorEbPOKALc5eLQPraJ9hcYjtKBKILJPlQBOc0z4RmxGZYDK5e98Ri3feS2MYBWgboGnI8kQVk/74rCLAuZB1UtoEOqxfsIItgrR8Wgb5YuyibYY+WJ2dbl0Ed/w3JzweukXwJ+UNEUS8GE7AsiJWMcKqNptD/pLojWV9s2uHgiwUb8G1uRfDDfdK3SfARqUpBlBoGAdMGEykNPLE4bMFPE41DOgQPjKXToUqX+XQkXe9e+4BkawclBPboXHscsQVq9jZdhFAdOgUgkNlO5p3Ruv2IiOR4FK+aJsZn0pEMivE+TdfQDVwJQJwF/HA2NEhjtJfIHaNQ14fD5P5VP/bjRUit/I18g+67bdBRxNAhJ28DLlq9zH2A1AJYNJ/EXqKfXsNdKm6ysIZE8UfRgNfDheLI81nVhDDeqJKWxG/WeU7fg1KgxglUHdezIBHuduQ1i9jvVhFwX8fcRTUUQgjBMaiBXjACrpq9IJDI+YDWXYJKDcQjIxZRv2+mHElwiwIwAlpY4EsFNXrUy7F0j5UAxqRBvUwKiXVfCQ/VK3nW1/+A7uJ8ZjVGNfbmDJKFVfxgCl5pO/5S6/nc+mUySkClDLt19lCCsHNW0bvM69hrB6OraGXRQwA6o69jEvqwYtSoOyRa1sooLSY7Z4F70mUe3Q24aqp/QoG6WpZyP0cpfdIQPKZtMTU/mMWr79lzCCVYC6Hd7OfYawejp2hF0U8Pcbz6h6fbFUT3Q8ardlSVhjZSNfzB7PotEEuYpqh942VD2lR9koTT0bocdAZYAqJfuTL6N02y9gBCsH9RQDNdkQVo8zKeyigA1qHKCNYbD1NiHrCXgu5Ic9d4UWqFcawiqDuu/UDni7DhjC6u3cG3ZRwN/fqh0bKijaW9C1lB5lo1W1rZQes8W7aLeC+9GzxVJv1K9YNPW2IXyFQH1NwonX2K7/yyhL+BlKt9GwClB3oq/roClY1VewAjao5w9uDIOttwlZT8BzoWfU1NcksHLidQbqJSjdeoUhrBzU9ET0dR+yDCsJ6oWceWxf5j4weiTHO37EhyJ35R3yTMpm0wFQf2oI6wCorsOWYQ30t+l13a63I6AZAQaqMpsyULPYjLrlf1C6lYaVg5qRhH7XEfRZhDXgs0HVHA27UjcCeWxGfZ3PpiensV2/hJItPzGEVYC6C/09Ry3DaoOqOx62QScCeSuH4cQ0CQxSGdQFEko2/8gQVhnU5Izd6O85ZhlWG1Sd0bCrdSOQt2qYPKMqoGZ9LKFk0w8MYQ2B6nOnWIY14Iu+hKjbQttgRwBA/qphodk0bboEBmrxpu8bwspBPb0HPvdxWIXVBtVmz2oE2Ix6croUKjKoG79rCKsAdS98vamWYdUF9QKe7oj7CXji1Irty8IFDx2ClRmVzaahGXXjZSg2gFUGdf+ZffD1nrAMa8AX8TgapXHUYCvrWHml9CibFR/KupQeZVO2t/JK6VE2Kz6UdSk9yqZsb+WV0JNBFTNq2gy+6y/a8B0UG8DKQT27D37PScuwBvw2qFbGL2pdYkAv5NWiC+krf7XY9c+QcDIE6rdhBKsANRk+b5plWG1Qo9CzVnGxgiogZTPquY8lFH32HzKoFKwyqAfO7ofPe8oyrDao1riMWvtiBnWmhLSZfNdfuP5bhrCGQPX3pVuGNeh3RsVerqAGQHsLupbSo2y0qraV0qNs2mp0LaVH2WhVbSulR9m01ehaQi93zTCcFJAyUM8tkFC4/pswglWAegD+vgxYhdU0qGTmjQCQvVD1o4q7gcMR52eJjkcdX+n68gIv6XxrzfIOBD4uvgAE24Ap4o5QpindBWSq7tRkHuPlK9AETL5uIIbXvA90qO5AjcXXm0MH9KQJQHW/iFEfsGmsyvYy0Ki6lcfAVz4DdQafTRVQi9Z93RBWDmrmAfi9p2VYAx7zM6tpUInMG3AdEp2+DJitPNd+lOmbxaIGW8+Xyw8cX8gze7z6GvD8SOH3KcBpIQGFGC75Rc9XN4DMZ7j+l2cAS5WnS8+Vn/QekrACqp4v1q9113BfN7wOjPkhX74m2fQTn6NiePoprvGzacB0BdgErteyjtukh4Hp4kbGq8z7YqAyQNPekHDqDT6jFqz7VxjBKkA9CH//acuwmgPVIPPGXpHmZlQme/A8UJgGZFfz++SVETU9oAa+FD24gAlidj3rDtXKC/HyVfw6H9AxB4HiTWJwV4WnK4qHr5Zu4A7RlzI/gv4i4etG0/mgokD1tgO5uUC3B8gQt4FfeZDHKeFyrr+4FQPpfUaYnlhCoApY2a6/YO0/wQhWGdT95w4h0HfGMqzBQGf4ICvvdAcgMvOGD/hABPlq5R74x4F9ERlYdPWo3WekL6VxAKrm8mCPy1FVisU4+ZLTCY0VfWOa0jeBHPN3a0bBE2ppZL/6gTnCz0dFQIHIZMJ8Vqp2ybH0a+dVAvr7gWZ2KNEPzBK+dnYhiDZh/xrQZG6vVLDmdnlGZbNpaEZd+yVDWAdA9Z21DKs1ULUyb6gybUjPArvXiI7/AqhTHWNZDrKWLz7SQfQAD7Fg/xxoVPlQQIiXr4yneV++PRVYohzSTOD398fbl/scMEwAxNqvlGpV/yz3Kwg05QCJ7wi98YCrH3hDDWqrsJkHNW/tHfJuX971z+a7/vw1l6DAAFYOatZhBPozESBg9Xuiz7OaB1U7y4c8XoniUzujkn9ildnhmGqXbCnIhC/m0Jsqgrsh/BguJnj0fPUBk8WApvdC/nCMEO+Llawj1N5Ay6bnix0xeYCmeqCjHXAreQVuA7qUTmnp6dkCQMEuYN5KObGc3HYlH0KhF9j0Ax6/xa2Av1TEcqTpXX/B2jvkmfTUbAmsyLv+VRKMYOXHqAxU/znLsJoGlcq84UgUnb0eeFvJzXQ10BDjbED5YmNTOJn7m1ahGinVopUPBeUreRj385VXgAXKl6kHTA9o1K5f11cA2D2Y+7ppDjDup3x5+HFVp6yAGgT2iCQW0kRg5iNcTxoLdmiPxuXi/WPAzBF8+Zr94R96IoYMVGU2lUGdL6FgFS8UrALUIwgyUC3CGgyoP7KquKgaapx5IwCceFN0ns06VwPbI54NoNJTeeGLKpuxLwCHRLa7BOO2n5cvdpps5hBVv4YCKRGXnFVtPy9fvlrg0S8N+Bq+DOhSHTMycZO+5HZEneIbDeQouaW8wFZVHlbJ2ukpeUYVs2n6HLHrF6AyYPVglUE9mHUUwUCWZVjNgBo1ALFUWAlyLPrqbWxf6mjEtkzEMGzXz0CdLyGfAWoA68CMGsi2DKsNamzjGNqKGFByBgwJWFj4gviSd/1iRj01R0LmRxLyVxjDymfUc4c4pBZhtUG1AIrWql8QeC7khyJ/3VD5SxSDVAE1b6UxrALUgwj6My3DGgyySzAaf9QAaKxuWEXpUTZDYY0VKD3KpiFlWEXpUTZDYY0VKD3KpiFlWEXoaYK6XIIRrALUAwj6z1iGFRSorLHxLHrRiacPRcv2df5jpxNDGVQxm6a/yXf9ecsk5BnAyo9RzyUj6MuwDKsuqDqNtKvtCESBOk9CLgPVAFYB6j4Efacsw2qDaoNnNQL5628DOy3FZlN5Rp0vIXepMax815+5B0HfScuw2qBaHSZ7fQZqmoD01FsSMhVQDWAVoO5GsD/VMqwIRvy+0h4HOwIGEShYPzQ0o2awY9T5EnKWiFmVgJWDejYJwf4Uy7DaoBqMim2OikDhZ7eDzaRst6/MqAxUI1jFjJqIYN9Ry7DaoEaNg11hEIHCDXcg/W0JGW/xIs+oizmoFKwc1DM7EOw7ZBlW/isFjZYpp3ni+arhRq6Kpw9Fy/b1uZ2eKtw4HOnvSkh/R5KBzfxQQjYD1QBWsevfDngPWIaVBFVvsGOpZwDp/VE2vW2oekqPslGaejZKj7Lp6VH1lB5lozT1bIRe0ZY7cfo9CRnvSsh4h59HzV5kDKsANQHw7rcMK9iPkLX+iIZqrW5YR+lRNkNhjRUoPcqmIWVYRelRNkNhjRUoPcqmIWVYRegVbf0jTs+VcPp9CRnvSchayCE1glXs+rcg6N1nGVYbVMMho1cgBvRCXn+/kL5KEkbgzAcSzsyVcOZ9CVmf8t0+2/1TsApQNwGePZZhtXf9NIeG1osR1G0jcfZDCWfmSTKwDE75Wz8DlYBVgLoR8OyyDKsNqiGK9AoXIail2/+EzHkS2JcoBiyDk12ZMoJVgPoZ0JtoDVZ/BsLvq1CNSeQAsF+M6yY0ENt17BLfNBeF31LMzJF6KldRNspX1C/XBwNHOtRq8fVl1Od49SvUAw8w6xLehxifhSpLUTFkz3V9NPIHR4NN35pdsuPP8n1S7HeoDFj5lNQyY1g5qKfXA707LMOqewk1cgCohAbsefNJ0wSkLADnCSrlSy+Jg/rXipFtD0Gg8YGhfFE2RTNevjhdQKrqFpHzAZVo+8C9/NcCk17jCT1mbQP6lE5pxEllKt15j5xlOmsB/3W/DOoKfq2fmlllUA+cXgu4EyzDiqDJ+46ohAatyzikg+4SsJ4nqLq+TCansAKPri92tyuRxEEZuHj5YnotW1Qfdgk4H1Cptjs3h/v5xrNAqvkUTGWJ98pPQsn6hCdIY3Cyn/jlGsAqQF2DYO8Wy7CaBlUZmKiEBmxA64FyFxCHx6ArbuRXLV+hFSKTOIQMfBBUb8MW9cCifFE2PT3mVM+mpecrE/C8BOwQNy+eD6hKp7V8lYsEF5e9ACyZIvzeDDSZu2u4LOk+ZLNTUgvZM6bEr6ZWGsPKQc1YhaB7k2VYEbSSzU8joYF6lxtXUClf+skp5PHRA0QXHsoXZSNgtOLL5QZeEceMyRXARpGYbV4R4DYHj/aHwqDtcrBUCUT2qX6gRMSwbNf9yFosIWsRPzXFZlN2Yx/7hT81swpQVyLo3mAZVnOgUgkNlAxxiNOMauSLSOIgB94KPJQvD5HEQdVnYkDD4SF85eWJWS3yC44EzK5WeqU/Q0d9KAhfLAFFwTpgwmQgp5cnDJkp/B5SXfwh+lW++0FkL7kkBGseu7FvtTGsHNT05Qi611uGFcGI+9SVsIQ11CChgbJNXGZUA1+6SRxU98CHtV1pnHgNsxG+egibauIJhzFGX2xGPZoEJOzgZcpXOZRjNwCVDCbxF9Z2pVLLZtD27InigzEa+HC8WB4bfgKI8FWx5yHkLP0Scpb8P2QvYYB+BQWrvmwIqwB1KYI9ay3Dag5Uliu0A5gtjp1YJyR1QgMRrLiAqu/LVHIK1hQiyFE2ql+UTeEkXr4UPfZ6WGQ5OZ9jVLLtbmDJKAEoG8sxQKn55G8Vex9G7rKvI2fZP8nA5q/+F+Sv+WdDWAWoSxDsWW0dVtYhrT9qALTWN6qj9Cibka6WndKjbFpaRnWUHmUz0tWyU3qUTUvLqI7Qq9z3CPJWfAN5y7+O3GVfQcGaf0P+2q8awipAXYyga6V1WG1QjYaMthMDGjV700rG1i+Ir6rkx5C36j+Qt+obyFvxdeSv/Q8UrPt3Q1g5qKc+RdC13DKsCNgzqjEhxBpfEHgu5Ieiav9oFKy+FPmr/hP5Kxmkl6Jg/bcMYRWgLkSwe4l1WAMRJ3qVMaEGQFnHyiulR9ms+FDWpfQom7K9lVdKj7JZ8aGsS+lRNmV7K6+EXvWBx5G/9r9RsPq/ZFgL1v8XCtd/xxBWsev/BMHuRdZhtUG1MnzR6xIDeiFnuQvpq+rgGBSs+wEK1/43Ctf+Fwo/u4wXA1g5qGkfIdjFZlWLsAbaooPPatgAxLtoe4q/HyN47H6Zi7nOeFUfehKF63+MorU/RMG676Hos++jcMP3DGEVu/75CHZ9bB1WPVB1GmlX2xGoOfwXFG24HIWf/RiF63+Iog0/RtHGHxrCKmbUDxHsYrOqRVgDrXbk7QhYikDNkadQtPEKFG28HIUypD9B0UZjWAWocxHsnGcZVtigWhoke2Wg5ujTKNr0cxRt/F8Z2OLNV6B40+WGsMqgHjz5PoKd1mFFoMWOvR0BSxGoPTYOJVt+ieLNP0fxpv9FyZafwwysfEY9+S7gfM8yrDaolsbIXhlAXcqzKNn6a5Rs/RWKt1yJErkYwypAfQdwWocV/mY7+HYELEWg7vhzhrmD2AAAIABJREFUKNl2DYoTOKwlW39pClYB6luA4y3LsOqCGu9TOPYpI3OnhIziroeU0Xax2HV81af+DWXbrkXZtmtQmnCVXMzAKkB9E3CwYg1W+CMeBak0jgJLWcfKK6UXSxCNttFrG9UOvW2oekqPslGaejZKj7Lp6VH1hF79iedRuuN3KN3+WxnYkm1Xm4JVBvXgidmAY5ZlWG1QqdEyYSMG9EJeLbqQvupPTETZzt+HYC3dfi3MwMpn1BOzgI6ZlmGFP+LBZcrYUAOgrGPlldJjtngXvbZR7dDbhqqn9Cgbpalno/Qom54eVU/oNQhQy3ZcJ8+qbGY1AyufUVNnAh3TLcNqg0qNlgkbMaAXcpa7kL4aTk5E+c7rUbbzOpSxQwBxGGAEqwB1OtD+umVY4W/QHg1qALS3oGspPWaLd9FrDdUOvW2oekqPslGaejZKj7Lp6VH1hJ4CasUODqrZwwAO6vHXgfaplmE1DSr7gbVu1pAAkL1QBdvdwOGInw8SHdeE9IG1QL0X8u0nydMHtJ8ujQpvACUDdubHyJdagepXsA2YIu4IlXXvAjLVN0zFcNuLXgwDTcDk6wb6cc37QIfqDlTWZiv9CvVRK/NKH7Bp7IAvi89CbTw5AZU7rwcDtWL7b1CWeIOpY1YB6mtA22TLsMJfH+pS2EJkUIjMG3AdEp2+DJitPNd+lOmbxaJBfYc3Zf1EYMxavpx4g/BxP/DcC7y8tJw/EblSeR69amYO64zqjZV+ZT7DfX55BrBUebr0XEB1E6oleHRj6AfWXcN93fA6MOaHfPmaZNNPfNZuR1A780rLOhHLh4Hpw/nyVeZ9MVArEq9H5c7rULHjdyhPvCkKVnb6KvILlgB1CtD2imVY4TMJKpV5Y6+AaFQme/A8UJgGZFcDfgIQlSkaVBVw0jNcZ/mPRHBVtvF5CAZORddHwhjpS/2e6lfx61x7zEGgeJPwsyo8r1YcfAVZjto7RL/K/Aj6i4SvG03ng9IEVS/zSsLlXH9xKwbS+4wwPbGoQa3c+TuUJw0yBSsHNeXvQOsky7DCV6cetoFlvQGIyrzhAz4QQb76ThHgx4F9Eedn9fSYR2bTLLt4e5o+1bCP4cC8pLPtQE/Cl/TaEdUvNpk5gbFq/W8COebv1tSEh7Umylc/MEf4+agIKBCZTFhbK1Wfdr22M81Im27mlX5glvC1swtBtInYfg1oMnfLeVPaBFQpM2ri9ajYdbMpWGVQDx2eCLS+aBlWa6BqZN5wqTJtSM8Cu9eIjv8CqFMdY0UGUo0Ps+mWsQD7LBXNDF/njQYE/XvC69Qaan31smY7NPrFMsBkPM31vz0VWKIc0kwAywkX+tPUE1ZNm44v9zlgmEYcqi3EULgNsgbqZl7xAm+oQW0VMbQA6kkOahXb9TNQd99qClYO6qEJQCsrFmH11YZiHrYQFmQq80Y/kHgV7+yMSp55Q5kdjqlGNEwvzJMGbL8A/qTMzhLwYTsCyFGtdwnATv/uHayqixjkCBeht2HtoPrlAiYLzfReyLvnEeJ9sS8kFzWTDVh420LvKV/9QMADNNUDHe2A+6zo122AOoddWNtDwnxBZQv6Nb5cMjsrs6uATT/gy4tbAX+p8DXS9K6/6eQEVCdeDwYqm1krdg8xBasAdTzQMt46rKZANci84UgUnb0eeFuZ+a4GGizMBkog5VfxBWrXNODx+XwGq39X+GABf5UPzmMi+GHbirqIcQy9ZeuG/gz6lTyM+/zKK8AC5cvUA6YHNBxiwhfLyrJbfOhumgOM+yn3O/x4qKXyQljbw03hvgwyrzQuF7F8DJg5gi9fs9/0Fze2669OvA7Vib9FVeLvUblnqClYBah/A1qesw6rryaix+JtZFDYaRzdTCkB4MSbovMMlKuB7RFXvCL11F6ZLbKMWge0suOzXiDlwwj7egAe4D80tlN01Prq5ch2UP1itplDVL6HAikRKZAi9WL1xSaMR7804Gv4MqBLdczIdK34UrcjKvOKF9iqysNq8fRUc9rzqE7ioFYn/R6Ve283BasA9TmgeZx1WM2Cqu54LMtGQWb2eBa9NlLt0NuGqqf0KBulqWej9Cibnh5VT+gxUGuSfifPqAzUquRhpmAVoD4LNP/VOqx+VbY4dcOJhqpXM71M6TFbvItew6h26G1D1VN6lI3S1LNRepRNT4+qJ/QUUGsSfwsZ1H13moKVg3pwHNA01jqsNqjUcBnbiAEld9XGytFrfEF8taQ9j9rE36Im8VrUsF3//j+iygSsAtSngaYnrMPaXxUdEFZDBUV7C7qW0mO2eBe91lDt0NuGqqf0KBulqWej9Cibnh5VT+i1pI1HbdJvUctA3fV7VB0YYQpWGdTDB9hu/3HrsFKgssbGs+gFJp4+FC3b1/mPnU4MOajXojbxGtTsvglVB+8yBasA9S9A02PWYfWxc5/2nx0B8xFoTfsb6pIEqHtvRfWBP5uCle/6DzwJND1sHVYbVPMjZK8pR4CDejXqEn+NGgbqoXtMwSpAfQJofMg6rL4KO/x2BCxFgIFan8RBrd07BNWH7zUFK9/1738caHzAOqw2qJYGyV4ZaE17FvVJv0Z94q9Qu28Iag7fZwpWAepooOFe67D6yu3Y2xGwFIG2U8+gPulXqE/6JeqSh6DmyIOmYBWgPgo03G0dVhtUS4NkrwwwUBuSfhkCtfboQxGw3q15zMpBTX4EqP+TdVjZ7xa1/pTTPPF81fLD6uLpQ9GyfZ1/XHVi2HZqnAxqQ9KVqEu+DbXHRsEMrALUh4D6kdZh7SdA1WloTNUMIL0/yqa3DVVP6VE2SlPPRulRNj09qp7So2yUpp6N0Gs/NQ5NSVeiIelnqD/4R9Qee8QUrALUB4H6O63D2h99s5zcdqKhen0j6yk9ykaK6hgpPcqmI0dWU3qUjRTVMVJ6lE1Hjqwm9DrSnkZT0s/QlHgF6o7cg7qUR03BKkC9H6gbFgHrPcbHrDao5HgZGokBveCXoQ0ba2EFol8daX9FU9IVaEq8HA2H70Xd8dGmYOWg7rsPqBtqHdb+Eu3WEw3V3sCgltKjbAaymmZKj7JpihlUUnqUzUBW00zpUTZNMYNKQs9xaiyad12OpqT/Qf3Re1GfOsYUrALUe4GaW63DaoNqMGIGZmJA/1FnVA7qT9Cc9GPUH7sP9alPmIJVgPpnoPYW67D2F2uPhO4AaCU0EBIdu8Q3zUXhtxQzs64eZdPwFfWL/MHAkYiHusXTl17CCCVq8fKl6LE7F2ZdwuN1Ps9CJfV6gUcjf3A02PSt2Y5Tf0Hzrh+jOemHaDh2P+pOPGkKVgHqXUD1H0zC+uDAFSxLoOokNGB3PSZNU50OiQeoOr5S7hd+ngPefkos3x/jfUzKaOr40k0YoWxHfcj0bDq+ZEnKpqcn2qL5gdHWG7iX/1pg0mvAq68Bs7YBfeb65Uh/Ei27foDmpO+jIeUBNJwcG4K1ljgMEKCOBKpvsA5rf5GqdapFrY7rJTRoXcaBGXSXACcOoGr6CgDnVgMvzgJKWbofJ3Apmxm+BzSr7i/SarvSNS2bpi8AVHIKSo+y6fli21A2Ztdqeyy+nJvFOIlZ9RvPAqnmUzA5Tz2Bll3/jZak/0b9yb+gPu0pU7ByUPeOAGqusw6rWVB1ExqwAa0Hyl1AvB6DTvlSBoZlZEmbJAK+zFpWlpAGADO+ohJGqASswEP5omyKu3j5KhcJLi57AVgyRcTwZqDJ3F3DzvQxaE36DlqS/gsN7HJq2tOmYBWg/hGo+o11WPsKlDCEv6qCQic0UHUuDqCa8+UH9oq8UNIIoFp1nz3rhart4Z0Kt5nzpZMwQhGOh69eF5EwQhXfePhyq/TkPqgSiOxTJYAjfDnTHkdr0rfRmvgfaEh/1jSsHNQ9w4Dqq63DagZUMqGB6ubAeIBq6MsHbLpXzALPAA0RkFoBlfI1pxIo2AXMWwk0+3kCiuFiV1moypJGDKj6A0MmhZi1X/Qn8gsOSxihim88fLEEFAXrgAmTgZxenjBkpvB7qEf5+JEf9s60x9Ca9C20Jn4DjenPmYZVgHoHUPlLa7DW3QWYAFXOAHE0CUjYwcuUr/KOjN0AVLLOir84gGroq3DGwKA+PQ2YOgOYsQrojOUYlUrU4Ab23CF8TQRmPiKWxwKqiUcNoxKG0GsYWJSvdsBMfMP0Ql74QpiN8tULZE8UfRkNfDh+oF8sjZHyF6anVPLXzrRH0Zb0DbQmfQ2NGeNNwypAHQpU/dw6rH354a1Q3hENRVRCA7FRXEBVGiBeVb7kXKnKrMbapy7lMSYTU7tT+ZKro06FjQZyVLMOW4mKE2WL9EW1Q7FRepQtypcbWDJKFb8xQKn55G+daY+gLemf0Lb722g8/bxpWAWotwEVP7UOayygKoGz8koFkrJZ8aGsS+lRNmV7K6+UHmWz4kNZl9KjbMr2Vl4Jva70h9G+6xK0Jv8STWcmmIZVgHorUPFj67D25Wk3n2io9gYGtZQeZTOQ1TRTepRNU8ygktKjbAaymmZKj7JpihlUEnpd6Q+hfZeEtuQr0XTmRdOwclBZkq3y71uH1QbVYMQMzMSAkocFBrKa5i+Ir670B9GexEFtPstANQerAPVmoPy/rMPal6sZk3/UINv90h7uqFriQ9GV/gAcMqg/Q3PmJJiFVQb1yK5BQPm3rMNqgxo1RpYqiAH9R/1QdKXfJ4Pafug6NGe+bBpWAepNQNnXrMPal6M9LmwA4l20PcXfjxE8dr/MxVxnvLrT7uEzasrtaDn3qmlYBag3AGVfsg6rVwdUnUba1XYEutPuhpPt+lNuQ0vWZNOwClB/D5RK1mH1ZtuRtyNgKQKutLtkUNtTbkNz9t9Nw8pBTfodB9UqrDaolgbJXhlwnRqJziQJ7akj0Jo91TSsAtTfAiWSdVi9WXbs7QhYioDr1AgO6ukn0ZLzmmlYBajXclCtwmqDammQ7JXZjDo8BGpr7uumYRWgXj0AqhVYvefs2NsRsBSBnlPD0LVTQvuZJ9GaOx1mYRWgXgUUS9Zh1QM13qdw7FNG5k4JGcVdDymj7WKx6/jqOTWUg5o7Ba15M03DykFN/CUH1SqsXvb8Uo0/CiyN1Q2rKD3KZiissQKlR9k0pAyrKD1mi3fRaxDVDr1tqHpCr+fUbTKobfmz0Jb/hmlYBahXAkWSdVhtUKnhMrYRAxp3SI18GbfW/BqEL3faYHTvlNBWMAdWYJVBPbzz5xxUq7B6zmo3nmio9gYGtZQeZTOQ1TRTepRNU8ygktJjtngXveZQ7dDbhqon9HpP3oKuvRLaC9+0BKsA9QqgULIOqw0qNVzGNmJA4w6pkS/j1ppfg/DVm/YHdB/4f2gvfMsSrALUn3JQrcLqOaPdeN2GaiVICADZC1Wzx93AYfO338oDqtmKSF9eQO+x51neAQXdtlO/yI/0BSDYBky5XtWvu4BM9X0olJ6wsbZolQdTmQPgf1X2p7cBjgDQ3wSsekJ7u4Fehi9F9ZlKMtEHbBqr0n8ZaFTdIcGUo/QG3PWm3YDuAxI6it62BKsA9SdAgWQdVkugaic0gOuQ6PRlwGzlceGjPoekEH7g+EKeMIElTXh+pPD7FOCM5Z4pJfg6/coUd7p+eQawVHlo71xAdW8fNaCagMrQzhGOVaB+N5HXLX8BmH6ALy+9QgWTAFppcuRrBFhkkomWdUL3YWD6cL58VbLph/Z6Tv4enam/QUfxu5ZgFaD+iINqFVbP6cgu8/cRHZcr9RIk7L2Bd3YUO4MQAArTgOzq2O+1Z870fIVa6wImiME7q3rcOrNrtV3ZTsum56v4da415iBQvIkvS6vC0xVp6al9MXtYuRFoApBdFz6jzmuHH8rjzyUgGwj2bYnY1kKOWSrJRMLlXHdxKwaAHmF6YvGk/Q5daYPQUfKeJVg5qDt+MACqFVjNgqqbIMEHfCAG4+o7RWAfB/ax0VD9GQ2oalVTSSGq5nJf4zR+/RUnX3ImlrFq0L4J5Ji/CS4cUKGT6AIKpgISe0K2aka9OxPyzYvD2HqPAF72OT8dO6i6SSa8wCzRlp1dCKJN+Pga0GRur+RJ+w06z9wHR8n7lmAVoH4XyJesw+rJUCMysKwabDpRQx/wjjKYzwK714iO/wKoUyU7UOkNOBFLKhvti+sF0QM8xHz+HGhU+VCEVXpKVehVZTP0lfE078u3pwJLlEOaCfId3Vp6oTplgflSl6cyEWBTJav75218rTtV63ycxutas4BWBmp6+PaqtisuQq+UDeokEw7gDeFTBrVV+LAC6tVwZj8DR+lcS7AKUC/joFqF1QyoVKIGliAh8Sre2RnsKYD9wBwRiGOqXTIVSJWNTNSgJGPwporgbgg/rlJGTaWnVIVeVTbS15xSYLLoR3ovT0AxQrwvViW9UOmFfCgLzKYuelerH4hYj23DMtb3rg/f3siX4pfN1LpJJlzAph9w3cXs01AqfIw0vev3pv0KzsLX4SibZwlWAeqlQJ5kHVZPeqh7YQthQTFIaOBIFJ29Hnh7pli+GmhQzXZhemGe+PqhKgNfbL3CyXybaToPc4uXr+Rh3M9XXgEWKF+mHjA9oGGQsjZJvwK+ey0vT6bwHk8dLOK1gb9f+gzw6j6+/P6/CJsKZG6J/h/ZZyrJRONyofsYMHMEX75mf/iHPlJP5dF78ko4yz+Cs2y+JVg5qNsFqFZh7TUDqqqVbDEqoUEAOPGmKqhXA9sbwzciOi4PaPjaA++ifAE4dBv3ldA1sJ56KV6+WAKKmUNU/RoKpDjUnrgtvGbgHWuHblkbfozK1nsjlX8BdVcD85XMiBEaA+rhS1F9ppJMeIGt41Rts3h6KvsxOCoWWIZVgPrvQK7EZ1UrsPaeCu+w8i6q44ohxldKj7LF4o7So2yfhy/mL55Fr40XsF89NcvQWfGJZVgFqN/goFqF1QZVb+jN1VOAxBNQRUuvVVQ79Lah6gm9nsadcFYutAyrDOqRbV8bANUKrL3im2Zko4mGRq5q6j2lR9lMiUesROlRtggZU28pPWaLd9FrFNUOvW2oekKvpykRnVWfWoZVgPoVIEeyDGvQBpUaLmMbMaBxh9TIl3Frza9B+OppSkJn1SLLsApQL+GgWoQ12LNXu/FEQ7U3MKil9CibgaymmdKjbJpiBpWUHrPFu+g1h2qH3jZUPaHX07gNndWLLcMqQJWALMkyrMHWqdrNjXeAiY7HfTBtX/H5gGiQEQz0oatmGbpqllqGlYOaIEC1Ciu7IyCgc5pHo6F21cUdAW/XOXTVLo8JVhnUhHWXAudihLXuoYs7+nbvTUXA3++Aq+EzdNetiAlWGdQp7/2BgxorrI3j7JnV1HBdnCv5+5rR07gJ3fWr0V230jKs59I/gAzq9x97dgDUWGEt+w7gXIygXs7Ui3OMLtpeBwP98Hnr4O08BVfjerga1sFVvyYmWOevmctBZbQmrBe7fwZqrLBa+YmgVvqgihiSCaufit1wF9DAnop9H9DInjA4Cmh6GGgaDTQ/DjQ9ATSNBZr/CjSPA1qeA1rGA60TgNYXgbZJQNsrQNtkoH0q0P76/9/ed0BXVWZtn+XnjLOmrPm/b75vnBmn6KiINImOKCDKKCgoUhUBpfcuAtKkiPTeE0pAaigJLdQQCL33XhIS0juE9Nx7z/Ov/ZbT7jn33kD4/3/9C9Y66xaSu/Oe89x99rv3s58N5IwBcsYBueOB3IlQ708G7k8F7k+H+mAm1AdzoObNg5q3AOrDRVAfBkN9uBhq/jKo+cuhFvwMtWAV1MI1UAvXQS1aD7VoI1AYDhRtAYq2AcWRQPFOqCW7gZK9QMk+qKX7gdIYqGWHoZYdheo6DtV1Eqr7NFT3Wajuc1A9F6B6LkJ1XYDqPg8PHWXn4HGdhaf0DNxlp+EuPQV36Ul4ik/CVXIC7uLjcBcfg6voKFxFR+AqPAxX4SGUFRxEWUEMyvIPoPThfpQ+jEZp3j6U5O1ByYPdKHmwC8X3d6I4dweKcyNRlLMdRTnbUJS9FYVZm1GYFYGizHAUZG5CQcZGFKZvQH76ehSkhaEgde0jgzX+yjz8usUEHagE1gs7BUifgvUpWP8fAWv976aZQcpiAEVRps5/8/HDgKee9alnfUzPeujwAnuASqDS43MtB/v/IeMvPH3+9AxU4Bn4e/spT/FXgefz6Uc9PQNPz8DTM/D0DDw9A/8/nIGGPTtjxrh3sWxKLSyf9QFWzm+ENSHNsC60Ldav7IKNYX0QsWkwtm79Adt3TcCOqFnYGROM3UdWYs+JCExevwG/+WqW3xjjuZaTsGfvVKRenYRsoUOUeWUsMi79gMyLI5hkNk3NSDv7HR+adaofG/Caery7mO/eCUmHOyDpUDskxrTBvQNfImF/C8Tva4b4qM8Qv7sx7u5qiLgdHyIu8gPEbquLO1vfxe0tb+NWRBBuh1fHrY1VcHNDJdwIewnX1/0V19c8j2ur/xNXV/4KV39WcGW5givLFFxeouDSYgUXgxVcDOHP6b3LS/n/Xw1VcHWFgmsrFFxf+SyurXoO19f8DtfX/gE31j2Pm2Ev4Ob6v+HWhhdxe+PLuL2pEu6EV0Hs5mq4s+UNxG4NQty2txC3vRbuRtZG/I66iN/5PhL21Me9vR8iIaoBEvd9jKToRkjc/xmSDnyO5IPNkXyoJVIOf4HUI62RerQN0o61Q/rxb5B2sgPST3VGxumuyDjTHZlneyLrXG9kne+H7Iv9kXNpIHIuD0LulcG4f3Uocq8Nx/0bI/Hg5g94cGsM8m6Pw8M74/EwdgLy4yahIH4K8hOmo+DeTBQkzkZh0jwUJi9EUUowilKXoChtGYrTl6M4YyVKMlejJGstSrM3oCRnE0pzI1B6fytKH2xHWd4OlD3cDVf+XrgKouEujIG76BA8xUfgKTnO02hlp1FQcAbrYnY7Y2jLsk9xI+wf7ALRhaATf2Pdn3E7ogo7gXSy6OTQicg63xe514axBdEfT39oSW44+yOyco+i9ZRVjoY6T56BnNuzeRfizanIvTUN9+9QV+I05N6cgpwbk5BzfQKyr5Hi21hkXfkBmZdHIuvScGRe/B6Z54cg49wgpJ8diPQz/ZF2ui/STvVG6smeSDnRFanHOiPlaEckH2mP5MPtkHSoDZIOtkbigZa4t7857kU3RcK+zxAf1RgJexsiftdHiN9ZH3d3vI+4yLqI2/4OYre9jTtb3kTs5jdwJ6IaAxYBjAPtVdwOf429R/8Xu7kG7mwJQuzWNxG37W0GuLjI2ri7ow7u7qjHQberPhL2fIR7exrgXtTHSNz3Ce5Ff8qAl3igKZJimiPpYAskH2yF5EOtkXL4K6QcaYvUo+2QduwbpB1vj/QTnZB2sgvSTnVF+unuyDjTAxlneyPzXF9knu+HrPMDkHVhILIvDkL2pcHIuTwEuVeGIefqcNy/NhK51FR3YzTu3xyLB7fG4cHt8ci7MxF5sZPwMG4K8u5Ow8P46ShImImChNkovDcHBYnzUJg0H4XJi1CUEmIB5s8oMYFzPUpyNqI0Nxyl9zdbALoLrvw9cBVEwVW4X4D0IFzFh+EuOQp36XF4yk7CU3YKnrLT8LjPoaD4HF7sZtn979zQnXkh8jgVAVZKMr/1bYgXWLtNnYUHcfNZz0xewiIUZ9A3MIx9C+mbWJK5hn8r6ZuZuYp9S+mbWpzxM4rTV4hjOYrTQ1GcFsq+zUVpS1GUSscSFKUu5ic0JQSFKcHsBNNJJg9QmDyfn3TyCElzUZBIF4KO2Si4N0scM1GYMIN5EOZF4qehIH4qCuKnIT+Bjuns/wvuzeAehn6Pfp8dc1CUOJd7HGaDLjDZJft0oYPF37aY/53s710q1rCMrYd5JrlOWjNb+yp2Lpi3ylyD4qw14nyFoTh7PUrZQR5sI/Ni5MnIaZSQN8slwNCxRQBnG0of0EEeLlIc5Ol2ouzhLnGQ19vDPR/zflHMA7oK9guQHdCA5i46yLwiAc5FnrH4CAOep+SY8JLkKU8Ib2kAousMPOw4y0BJBQt2sCLGBXjc/P2iovP4dZvZHEf1BvzExlEnHmxXoWCNOuPtvjNjid29CPlJy/i3L3uDONHrTYBlF4OBVQesZ+VIlN5ZXA7AStAGozCFg8UesBK0ZsBi3lcouTRegFYCdqojYAvvBQZY+gIx75RiBqx7xVCU3FpgACx9IVfwLykDLN1aCbR0TsoLWLoFGwG7Feq6SSi7txFlDwyAzasIwB62APYY85gIXwhPxn6z5/QBWPKqdJy9fYQDdXTwFCb6n3qyDxIPtkX87kYslqsIz2qM2/vMnsu4iHn3FqM42/Dtp9sFAywHLfew3MtywK6BO2Y6ypRn4enWVniZ8nhYI2Cll7XzsDpgsX0A52U2+QfzoLqXJcBK0HIPm58wQ3jZmcwrGwFbwDys9LLSwwovqwE2BGX7xrP1qV1bg90hKO5LC+WxH8V/jwxYcZ41D8sB6z6ygNvr2Un3sH4BK7ws3b4LovnBbuPSw1LMafSwOmBxOozZQ7++AXrYc7wsTEB2n8Ov2s6Bsm33fKYDRONUfIF1Vs9KaPTGO2gcVAcLhrzPAnx/MasRqIePLkVeYigKUn5mQTbdmtgtim5V7HAGrEd5Gxg3H26lEjzbfkRJhgwL6PYoQUsXdLmPkMAJsAstIcFcM3l4VVdxm+eApNs/hQH+AMvCCRES+AOsR6mprc+9eRQPZSikCRiwqy0hQZglJJCOgYcEqlJLs4edMwxhwXaUaWHBDpQxDyu9rAwJyg9YPFNXtxe9jHlY+5DgtCkkYPGq6zT3qJH7Q1lrACms+QLrq799By8oddhR6bd1nDdYseNZzEagMQH1xDI8TFqBoowwHjNR/MTA6huw6tBuQFA7zkTaFg23Uhllscv4bTAgwC4zx7BaHEshgdHDcsCi01tmoCoKSq9M1GJY2v369rAyjuUScLtZAAAgAElEQVQe1h9gPYPaWtZXCSU354qYm3bVRsBKLytDAh7H6iGBM2BL2V1sI/B9D4u9KihLWGeJYysOsBg5yGTPo9SAJyMKFMfSJooD9gTcxs2USwCWba5OchztPLyeMVyo6coXWCVI5aMxG5ByqJU5GyDAagLq8RXIT1mJwsz1/BvMAvwtPOB3AKzr4BzmRZGQolPmGvWGp3s7w8ZrlV8PW5y2jG9atE2XceOlAxa7hniBlLW7NHlJ3zSxjReB8PEBW7p3tO361C6teBxLXyq26So/YNnGlDaobLPKPazT+VR7dBAbL9p0GTde5QWsCAtESIATK23Xhz69+W6/5KhPwHIQH+dA3X1iG6NtER3LF1glQOWjNXVlB1YjUI+cWMnoXsWZG0VctM0vYNktas5KHaT07MFDHgJsn8h3v1qmgO+OtSyBTUigAZZlCgisRsCG2INU9oCt7mGTKbACdrpNSGD0sMLLipCAvAsc1ueKGKFnCgIBbLoIgyhllGHOFLCMStZasBDKwZ66YyrPe7JMgT/AGjMFu1GWL8MCypESWDlg1V/UcVwf9i1hmQKWnhKA5flUyhKITAF53ZKjHKhRZ/cyLiJxDI1gjTvyPbYHd8CWuS0RPr2BdtuXQF076kWEja2KjRP+hcsb6rKY1QpWE1BPrUJB2joUZ4WznSalSHiqxAJY2p3mRgDf9wQa9TaDVL7SQoDl5U9t0SZFelgDYNH5Xd9AVRSUXZ3ikNryB1hjaov/rPvb1n7W9wqKr89i4YmW2mKAlV5WhgTGjZdI4YnUlhGw6pCufuxVQend1eVIbVEM6wxYjBjg055HqQ5P6m6WS3VKbbF0V9EhDtR95/cxgm1ZfozmWU/unoTXfv+BFzglSK2PFLNunlrPC6xGoB49tYYRakuyN7MFoldnqFUbwPP6R7YHbZzIezr+a9QbqvIveF6rL44P4HnNfJSdn6flYtFcKAdKD/kEHj2HR2ohgavT53D/s5bzEcD6aJPlfqU23K/U5UelunAbjpIz08XGaxkoY+B59T14Xq2nH5XrwUPHax/wW7Df81kLnir/tj9e/wjuSys056L27MSuH11D89EQatWGgdl79j2o1Rt5H9UaQ63WGJ6bm1nOluFo39nNUF2cDU4Mb/Ksk4cNChikErQjv35X22Bxz9qJfxMEWo+dXYfCjE0oztnCEswIHguPUgc4eNr+MMaldmilk+70uwMmsovDc448F4vJ7fx6zMdtvzYWD9wzuz3R9RFgiwy5WPfc/k/UHgHYmItVg394ovYIwO6iGFChgUEo6lQob4UwgHVN8ORyA3VC5yBTuZXAavKo59ajKDMCxbnbWM2XKiFupSoHmx0QH+M9+gJ4RAxrLB48LhB9/v5aimH1XCzt9l3KP5/Y+lwRw0VYEKJtvNhdiL68FfyPzifFsLzapRcP3EqVJ7Y+FsMWHmAVMg7U4zOg5q+wBSt51klD+mLCt928gDuyfT0Mb/M2vv+iGsa1e9mWG2AE6rFzG1GUvQUl9yNZiY4CcISM01MXvk5uTqTwhsFmHXy739kWzUICKi3KHS9tJgiwmPKNf6/aZjWQToK7LuDKOv8/L0IIVhPXyrMcsO5Z3X2vj+QpJzYw2BgAJBgnUtgscFs0KCTgVTZz8cAzp69ve9rH2Uxy0f7P8oSdz1p6idZQPFAXjfBjz9eEFYsd+ZLs/aIOKx7QpoyILAxH+w6PYk1rdmA1brDkLV4+ahWsdX9zJLKYgHo+nDWElTzYqdeR8/f48aqFwPbRhgsZAFCD2kFdOMyx2uXTKyqL+OmK+hFoPZkL7+YsMth3kNqZ0FIHDpE4DIB1Ka84e53T3fhnVx4NjJGADTcL48oLKB+D2sE1u5ew4V3t8u9VHSa5yM+3PtL5XPC9YERR4cBcPPDlVfWBFG8Cg0fxqTTjI4BSqxHDa8qZL5nAYlPiHFDRgQM1pg+QOdAA1l2s25E6GGXMStkACVD5aCq3OoDVCNQT5zezDsbSvF16CiN/L7D4R+dvZeZSfiHrSXFaP0A9eBp04niliype5mpXcXYYMLWDD+B9Agzso///xhK4sU9/7bABo125XfGAAEug0goWhuvBnpZkA5cvAw+LgVMTuJ0q+6w/pb8+eBoEfE6modKsd3nWPbuPsz36JKdJLroV/Rk7n5W1O5MkwMjiAZ1ndeFwZ3u+JqzoVvRnzF51rTxL3pQqZQxH0fvb8PZiP2CVAJWPKVZugA1YjUA9fmELa7ktfUh0r/16vbggCm6S/r50Q/+D5bOSZCA2HyiS86j8AJW+/YtGsFjKV3nWt1eVXnME92y7mvkG6qQvHVhbdFvm1S43eVW79cl1bpUZiS+AdIMsvPx/+RjUDu6Z3XmVzLE8S6VmB3uOE2qkActjUDt45g/mxRVL8UDyM8gZMK9qtz7HCSsOayRvuvhHzZGRNyU+Kwfq3o+BhDp+wdqyfjvNqzat05QRnP2B1QTUS9tYj3jZwyi4KUgu5LQxIjhgxECg/XDLWTK8DASol26wE1ZKlRU/5Vl0ExLpDh4Syk9cajwl1DdIFYWXOGX1SCvPGmmGC8HKpI7rU4G0S8C2KcJWP7PWvzwNl26wzRmVbxmJmYjMGs2QUxalh7Wz53eSi7QjH9n5rOxYPKD4X3pYDO3u+/qxzzROWLFMMaT/Z/aqmooHREWka8lwtH/nm8Cd3wcE1pgdC3Fgy3RRwSLW1Qj4AqsRqCcuR6I0Lwpl+dEs7cAY3gKw7tRIuJS/wlQqlSeMHgMBKgFhWB9ztUsUD3QCTDgLC9xxy50B+Mdgbvm8uBU7glkButS1LR5YebGUuC9T/mRZnwe4FgnMXg6ku/n0lEbCm1+32VC1Hw73wFYWmiEvHlgBW3yN2GZ/NNnzOclFTo0xnvP2w6EO7qjRDI3FA708y0u0ZXEr4FL+YrLHbkeOE1YKjJb4c7p+w/tpnFjabFNBqCQnnAN12JwmfGBvgGAlhQy9guUM1qOL/4sbEGg9cWUHU+BwFRwQdDCihMVwEm7hAaB3D8aw8V5BYEAlGiDxK52rXZsNIUE40PJfNmAdwM6vBwlglZXu/YCOYpSiDWBdxB8VjC0zkdu7POvu0sqyPhXY+bH4GwYC42Setytg43BofUWXfnIgcnsD1tO5ucVeAFNjDCef7JXeXqTzf22qXUbAerrTpJT5hk+gKYMDxfraA7P6ieddbe8YZM+VGG6qdrG7Y/YGgaM2o7naNI1B9wLrFKgFlLraZSkK+AfroHHtTEA9eWUX25y5CmN4C0LRIcYO5xzGGKB3T+DnLeaFylcBeFTK9bkSNzHWOi/PihIt9eywg+rXVJ7lgEXLWt5AnWUZGAyKAAzjGy1gdd1eqF9Ixht17jxQu3zhvT5KT/1kDEPaA5dsvA0N4FTqoPDyRN6/ZOLEGsuzMiyYBQZUp/NJ59Vuaow838JeeYjcRBTyvn6+JqwYjAl7jMht6Dxg7LqsMB1Hw+Y0tAHrVyIbUH6wbl/TVv9w8qgt5k0hnSPSOCKtI6rt8vaFwwKsh1gZjVWazH9/4K9oI7VrLv9GisX6AmxgGyq5sXJ43DA4YCK3u9J7zmmqQFYZ1A5lGwfZELmpVcbceUBxrPuVdx/bnnvzDxaaoeD82hC5qUz7+NdvhrgjEmsrkmdvMteYsdRjcjM8OK1YwoDygzVsw1DzByuK0nFuGNzFR1FWRKzvo7yvhoFVByy5/sf6R7cdFuPs5IRfIkywI5KlODQSjPCwFQLUrh/44cXqHrYi1uf+9iuNF0tg9MWLrQh7nu++EQwzIsBIEozkxJo7DyrCHob1Enc/6lzdxnK3xIYz7nW059UHfHtSe1GOJx8Mm/f5M23meH9oiwVTCh8eh4taYot50xdv/uIULqJx4dRa4FefPhZOsS0aaN1WK8+yZjViqNsAFjFicK/lVl5u8NZ+ISBerGv/5ApZn6dZI8aLZe0uGi/WG7DFu4ZWiD21eROtPMspkVbActBW1PrQ6gsDL3Yry99SG045YPhoP/pMhxAkZxKLm8sfamxuxuw+zkizBFosmxJAesMPjhNSoCq1tfIsSxZrnZVmwGKGGKr7uEBVFEYzlL1dnATjTeR2zxtUIesj/qqJT0C5VA2wOpHbNbNLBdmraUMzFBtFg4d1z/OTXvRz6dh/s+tXS+cT5G5mZW/apD4a+gL4rT/3DsXG4wdQWnKOa3aWnhE92xKwJwU5lkB8HOjX25tgG8jiLD9Dtx8mcEBtvqzld7fZw1JbcN4O4Ms63hupRwQtVbs4p4DzCdhOWLR7y94uT7c2FbY+Xp6dx1qztXZvC2A9nZpWnD3Z8s2aEUUHrYXITRRDLwK45doE8pKuH68qUm9XOAuryJN7Q67FnCmLt89CRiKJ1M6GmjefSyjmL2HEFbVgNdTCMC5GS0K0xZFCgDYKKhOePQLVdQKq+wxUz3kmNssEZqmbkIQF2EEiA2bAqjUaOwfidEtv1Jt3oRItkBjqThRAqmzsXcgrGwysErB7TIAt9y3eF4jDR7DEN/XYS8BaNQp8bjTKuT7GmtL4BPYaBT43UuW1Fz6MayL4AKzPjWI57amRkzU+ATVuUmu5CahNJ81EaaZUVyZl5WmPCVYSETjPFZBJBVn2cFsBW3aSt9Iav3JUjqMEt1KJEWixdAo86XuBU2uANh14cYDY/9b0y4CJwJKfTOVZs4flgPUL1Bnd4UlYC8RMB9572bf3ndYJVPvmJcX1sAOs10bDsD4iOrvnfYvSWwvhip4EtcXnvDjgsD6q5WuiGg6A9WWPyNyuGV1YTrZ41/fwNG3IiwMO9swEGEmCWWQKCXzZ4+sbhNLbISg7MAVo2czn+iQBhs4n5ahprRpQ3xg8U1dZzvkRKFgJFG8HSvYAJeQtpVQ3ecxjwmuSTLdBoptkuknpQkh1S/ULKSTAJLttAIuzG3ngT15yzkqWL2S9RKOHQr21nTd/kewL9dUYMgVYOhlo0IozyamqQTxMAm7vHnDJEq3sP5d9PESCOTTPHni9PgMuLtUURLQsAe0+728FpncH/mKTpmpJFDhOgNFJMDpg3bRxo42iYX3USasO6YTSc3MN7d7URsLbvSkuc88dAE99wZQ3rI/ysToBxlsFpnTvD172iBdLrS8lx8Y6qsC4ZnaFu67oBDDYo3wshRiyPMu1p3TAlkaN87JHDsbzXXuUnJnJKne6RoHeKuOe9x089Rt5XT/WuJnFVWDo94g+qQE18+4QIPM7pl1Pt3YURwBFWzlYNX15Amy0ri1vAuxZXVO+nIBF6Az2x7qUvwNtOwH7lzMJGOPGi5q+WHutDWCpXQHTiG1enXvg6h9r1S47wGJWXx2o9V4Htk4whAXG1BZXELHmYnE+BBjRWv8M2lBRPJXDy7OcBqd7WPIQdOHKqMTYsiU82yeUS7aILrZnfHf2GfQ5GrOftKC0dm8dsO7ZvYW9P4KyBGWbhvAOWrbpIukiueni0kVmfYJpKD42Bu4x3zBeAbP3yrt6B26it2yRbu9PUJt/CnfESFPnAZNbEj1qOmD11BZ9WdWf+rA2eAbw1z7QNArod+lLwoD6xZRJQGZvIKM/j0nZYASKQzfoQxHIuxbv0AYiEGB5TCqHIVBceooPQmCxKXlX4WHJy0ptITYUgUu1SA+LiBBg+Ux4qJfbEMOSaBZpWNkBVoLW6GGpgICTq4GvvhElWr08awQsVowAgZV1Smqbrj2MDKFnCrwBS3QzzcvKXGzMNBYa6CQYb8Cqq8cKPqeMYWnzJTde1CajyxZpHbRSf8pS7SqLngC1+eeG7llvUQ1X6CAQeOyI3MZWGQlYcy7WXDwo2TUcnqafCBLMLHirwMyDK/Rbbk/GsKS1ZVCBYbwHTWfLTy72wFQWGkjZImq1oawGA+qkdaP5pJCMflDzQ1nJVJ/kYQFs0WMC1hAWOIYEjwTYo7bVLlmelQQYjbWl0QxFa69h40VkCHvAUgHBvnjAatKiPMtFNYyA9ebFWjsP7DIFTAHG1PKtFw+8heG8Aat1AGhxLHUd6K0y/nKxujgcKcM8vmxReQBLX1gS1WB3jAQxvmffoUF8tE3mIDGCRoyfKVgBtWClYfQMedhNhrEz5GF3sZEzaimNnKFxM4cM42bIw8rdv8XDPiHA6nEsqcvRYeQTHNRCgvIDdpfvatcDI5/AP81QbrweDbAVI1skU1uPA1gza8ueyM02fgF5WFnx0lVg6MtGXxjmUfcd6g+kfcNmLql5cw0zk+S8JAfAbvwJSFvPQgIirdCMJP+AFbORhLygcePl7WHNmQJEBEPNOmBIbUmhAp6LtVa7eFjgAFjG2iJxL8GLtfGwWD8d7uQtlpCApBnNxQNTSCA2XtzDBg5YysWqq8agLDaUx2dGUY1yyxYZRTV8dB6Efouiq9NFt4BQIrQpHtjrbBlFNYRkpyORmxPIsaADSq/PKJeoBm3c8u9OFkA92IcND1MzeuqDvhhgKYdKQ75CDAO+VoJyqTg5k6eU+nQAijYbBnsRYPcIwO4XHpYyBcfFQK8zlkwBAdeaKRAxrBzu5ToLnN3E7fXtb4pjeQwr41hfgJWcAouHNdAMjURu9cQKbq9Xd1FAIGEwcy7WG7CRegxbTsCS1A6leIgqJ3Ox/qpdTOGPxbAyU+BLtshM5C6NGsvtdW7Beq9I11V6WD2O5dRBPYb1pwLjDFjsGMw3n01f5ak1H8UDXRgulP1NpHrNPWpMLz7tjqbasel0U3wAlhL/RDp+k3EPaZeGvZOhFtEkOm/Ayil0PCR4dMDiufd1ewdWeRcPTBuvxwcsk6IR6oGsgGCIYZ8EYI3qemrkJNtcrFGjQNt0mWJYC2ANKjBWIrdRPdBFCX3WjGhfPJDdBM6ANdIMvXmx9AUw5a3X9dIFjv0AlsKSh7E/SaB2B2g8Y2oHIGesYZwiAZaS/nKUInnYhcCIr/VmLta6+xqQEgq1UIxPZICl1BYfnWjMxaplB6GWyVysHJnoOxeL0YaWXNEqrGYe4gUEm+KBd6aAiDA6AcaaKSAdeb7p4kRujPzWtD7q53KnbDdkCeyrXdzDyrCANl0+PKyRF2unrhf3s99ql09FbiZbZFTk1oncxIjSmg2ZNBJJB800ZAmcAGtMbdEGy+ph7QGLjm+agSqlkUgBXGYKjIA1iGqQTHvenR/lrb8zkNwYSGtjmAFKgP3RMP+TADsDODqS5ehMJUyqaPRuI2Z+rtVLrBWQi8W5Lfb2+vUXFS8hsW0BrK/UFm249E2XDAk4LxYnV9nboyKCTfGA8wnsQgIDYKnvx0tnizZfW0CCuuyuZCwJk1RRj/aGDlq9eCDFzvQsQeCpLcoUlO2bYGuPug+0alcFSshrgsjWEnSTl3UvbgSsRUK+IH468m6NlR61I5DUEEj9EsgaAmR97wxYpZo3+UCo62H3GMOQ2ooBLH5Z39lezGq/5dnyAlZ99j1ne3uDeUOiX8BSektuuuwAq3ceqMo7jvbUyCmi5q0XD3RRDepVKn8u1qMEOdpzR4wQYNWLB4HmYil9ZfawnMhtuuVbwbqmh9+ZB/l3p+HBzdESqN8AifWBlOZAZn/DpGUC7HB9yvL3zfyos1UCkuf5z8UGWDzA6OF+7NWEmnWESWfL4oHGJ3iEXCxGfefHHqnP7fBZntU8rGMuVujlP9jOGhF9qxUKdT2v8qyzhLyv4gE16vm2VwklN+bw3KVW8aIdu3VIh2HjZal26XHsNKBDda9bvhW4TCCZDfyQ+gQUdughQX7cFNy/MUoA9UA73oHapQpQ6XWgEj1WAyrTUQOo/AY72C3KjxoclDeAanWAanX5UbUeUPV9cdQHbobo1a6+nYDqDYBqDflR42OAHY2AGo35LcqfPdpk1Whic3wK1PgUiN2pZQnQtx97T63xKYitpR1COS6Q9dEmS63+sfmo+jFUdjSE53qYRjNEr25M1Y6U7cxqd1z9LiB7ip26nlQvrA/XxWCRJQhj4sZWNUP2Wij6BWKPPK5RLZA9Z0qCpChYG8UnJ4pb9lygiR+yjtWDPsLrsgPf4f71ERyo0ftbA/G1gFk1n6g6G6rWMxcPlg5/svZqNNEZW67TIE4BNcg5KgAa40Qjk0s+96MeSGDlcSyvdpGQwpO056lS3yRb5Fkw9InaI+lLY7ULE/wIczwCMK0el3b8NM+Mpaeio1sBCTWBxA/gUZ535oXKC/YIj+yC7R4tOK16tcujvP7k7JliWFE8oKEOT0jtDntkDCtVYIQCzBOyx2NYc3mWWFlPan0shjWFBAv93tqtwCvX6zU9kHdnPHKuDpFAbQHEvw4k1AbmvKenLmwB6QEuGv/A5sB+7xZj068ScVZ5w7Z4gCWDfdgrAb6zodXRN/VCicmE6QXZe+59QYQxFw+I/KKlZky/JF6oWcCIdwwXoClwzqbJ3vi7pD737HsmjQJZPMDi8b7tedKAYYa27aDpQI6D3I20KdX1bKbKEFPLcX22ioilwPquhvUOAVLd0hJ/ZCnBIFvZIkxobvhd67WqDiQDON3K8jNO75t/n8qzNO6SRmEKj/o5EPcSEP8Wy6d6lP92/lbmSw2oPwM/DRd/QBtbQQFttcS6D+4ONW+O6BhYKKpdvHjgUV5zsOcGDi/kCnDfjwL6NxH2ugH3Ve3jvZ6QvRWzLbxYA2B9edVzvbiNX4wFlnwr7M0AbIRLNLtkb+lkUy5WkmDYOEXS1bL1qm5gdRC3UfsHoOM/+POgPX7V/NRFo2xkizjN0Ftdz4ciYsZqsca2wBghtFHDYp/0rub214deWGSL7L3kQOCWOEMmoDq9bwYpfvyUMbZoNmvOpUECqPto0/ECcLcakNgImP2287dyV22+sDbnSDIAuH4cuJjAdZq0K2d4cvA0PMqLUO/bFw+oPIvF/Z3taR+VDwwQizlbqL3r9YTZqykohvZEbiyf5Wzv5g98fR33ATfXi4u4wlmTldmrYdEooMKBrgJDEop2Xk5FAfCxWNMdN1T3DWGvDvDAa2X8DaZ2V9UgqGEW1SBerJe6ni9FxPBXuM2QTOgSkZ/pjofZq2SZ3aUXD6hNBBOtHrMpUHIfWLqEy0tqQHV63wJSRdFki2iQcPbFgRKoH3OFlLhXeZoqqQE8yv+yUZ9zATPFh9ZsLE5qB2B3msNZBb9AC7+2LR6oD8jDzmPVLg+VYu3U4OQnx4v25p6X5Dv2j+Tdls8QvFhJgPEGLCsh2thTcR/oajxxdB6K7W3Ru2Rv2RRT5wGxtqzVLiJ1e6+vDJggbM29AVwzlBrvWm6/8i8gkY3g0RbZIl48MKrAmNT1HBURy4Dxwv7WPKjIEtf0t0CauGORN503iHce+Jg/a+9V/wzk2t36nd4Xf8u4xppsEaWmsi/0F0CNagDcVoDYv/E4NbEe8G0lm3Zbgxqb0hvYsVIsrCqQZBNXXboBD7H2s0eKXKx9tYsAi+Ff2djjV4d5nq9oEa8DqTZ25EVk9mpYWmXsOw9YWZbaLaz/Tok26v8ZCSyWoc0AwM6JC3v+y7OHwIaC2dkrPA98Ii6QcZecYLNOqXYXiAoMTZSx2vOSRSoDfjQCNdMMVKHmp09GFK0yNoBFZ3GnNa5BcQKk0/v8bzEWD+5fH8kmmfMYdW99DlSmOxUExP8LuFwLLuU5727PbULHc+xdsMBNeoSDNleSTtTQBkDWYFHtMhQPqC8rdwITD8b9qcC9SXAp/+Ntj4BUckScwHW+YzeyN3pgQJ0HyD4G1vpiSkmVAsPEhTtZxNX1PhOvb5JUuuUfszeUtXvLVhkJWiufgI2psVMr9BQDaclATjZQKDWuPgLyLLboJdkjJRhNVEMvHmglWtF5QJP4vNT1vICqAuv/zs9tSCbgvi3OcxN+6yc1vyGdtakyvgBbdtNwN9DA6gRIp/cVoGNNrVWGigeUQ80810t41L31gFsKcPs/gPjXeKyaEAR0/Ku3OlvuNrGYd4DJ48TzmkCKtwdgnYl3erAWF2QO8g/Ynk287dEFuj6M2xkdZ3P19LeYvYzNghfrv1UGA0hBz6I+t+cTbuuXQ4F5cjP1pR6z6eY4TS5jv59WGdmQeBjo08tiTwV21Of26k4Aer7Knzc6bLCiP6X1uZI225Rn7TsPaLyOaX1eQAWQuozbVEiJ7zP+PGgvcwhkj4/y5HwCqVHg3XnAWVtoVkV8lvhyP4JHpUqVkcjNgHq2hwRqbeAmAVUB4v4GxL3MAdvxee92ZNpAHZ1o+INqAptT9bNpeMZyp7c7Auk9gYw+ArADBWCHivLsKCBnDJAzDuj5qY09ANFC7S7czs3oBpk9QeQOpPMAAwZ42yN1vXEfGtbXADhEgZb3P7LHidzEh5WcWCJzUyOirgIjSTA0VtGrvduVCHz9rG6v0VIgzz6jQfZsidyahzUDlgHV2E5uB1SUAJt66vYVPT1F9spD5EbzaobPIbA6eU6n92nm7GSNF0uAzb0+AhlnunGg7tvzDhdGI69Kt//YP7J4VX3pdw5pFe+LZvsObTS2fM7bXDK6AenkXQmw/ZhKIOt6zSLADgOyRwGVgx7f3t6phlYZohkaOw8kkfsoI3KzEqtt2sh2Nd5v0vqiV2olWl1Uw77zgAZ82aepvD/a9h2yt2eBRuQ293ZJEowuDKe+/uFj23NH0hhPYzPiGq0Z0eph7TdU0rsG+Li2p4nInXttBNJPdxFA3f0WcF3RveqdZxlg2a3U9owF+CbdVr97B0j7Gmp6B7+ArRB7I7tCLSJerD2Rm7fKcMBWiL0xw+x5sczDSi/LidwVYm/EwAB5sTt4h0KAl8r2x+j6De0ueLEEVg5YswqMPn+2QoDa6W0TL5aAmnGyswTqGxyoJOTLYlUF2K5wUQHbFQT4JlWImlfm9MHUrxhgkdZeALYr97CsTbsfENOxYuy1bsZaZdTCtQKwG0XngYXIfTq0Yuy17eKTF0ujvSkswJn1FWPvq6ABnPkAAAjwSURBVK/NvFiHzgPPsSUVY++LVn5VYIixBRp3pG2iAvSedj9f69c6LzZpPnKvDUf6iQ4iRt1VDbim6F6V4tWpind6I0B8aj9GO2rSrafugZQW9oCl9hcKCeZ/VkH2glirjFrwsw5Yu86DpWMrxt5z79vTDC1EboROrxB7xN6S5VkzkZu6DvTOAzZojrIEj/NPqut50Qy9idwBDZqzA6bNe0YVGAJq2vFvBFB3VgauKhys5FXp+EbxJtg+wqLZ7S7pEyD5UyDlcwHYVgDzsG3BPWxHoNu7FWZPfRgMlYm6kUYBAXYVmIeV4m7UeUAUQ+s48EdcH4lreHfQmjsPGMWwguzp5VnZQRttYm0RYIliWFHr86UCI3W20EKUgm2AV15PS9UuOgiwOVeHI+2YkNiP3vkqcEXRwXpdgfqi4hyIl1OdDZveApIaABKwyQTY5kCqDlj1tVcrzt7uoVDziE/gDFhU/3fF2TuwRlOCcQKsz41bec9nVIiFAOMNWOLAOm7cymsvcjobS+8LsOUFo8+fD+snCDCLGVBTj7aRHvUlrt9vAKtX4G9Qn1P/IUKDcwoQ+QzQ5Bm4lP/g7HFjOoQ8FKnrzawMJL4nyrMfCcA2BiRgU1p5B/4Ge4zIPb8ZENsXONgVaFUXLuW/nO0FdxMqhPOERkEw1IekUaCrwPhaHyNyL/sRyIwEzqwC2vpRDyQCjKXl2wpYX/aIyI1l0+DJjAbOhAFtOvpWK2QEGNmQqMsW6RoF0T7PJwNxyFi4ksLhOboYaN2GFwcc1PwYAcaPCoxP4JGXnfI13HeCgb3jgNp/8h3PsgFzSxkJhjxqypEvBVB3vABcUnSwRoiNFMWYmrqeAvRRgAMiO0CbLrnxovzrnV8B034B1H2OM/MpPqLUDwG34194teveOwKwHwBJBsDu/TcP/E32ngcG1wbOtuFyQywX29eci13wBfDvd7zt9fwMVO1SH0y3BSyOT7Kx9zowqidwKxRwkC1C6CSg4Rfe9gYMNOtsWVRgcC7Cxl5N4IdhwB3qQCC9WO9cLIEXH9vY69PLpwoMTqz0sscmI44YAPXaOhtFburr2gkEj4HaoKnX+igfa50qw2WLIpg4HPZPsgde9w+BM7MNvV3m1BYbS//fNhuvZtVAvf3UjJhzdRhSDosp5dGRf+JAlWD9SWF/rIu+CTSga6WIXymFRYeMY2nTZQfYQ78BRv0OHuWX7HPUV34nql01dcAmkIcVgJ31prD3G54liPiQTxFMbcMVXFhqqzPAcrE2xYMLfYCJzRmvgLVbVH6TlWeRO8kCWK4CgxBSjqsEF230WjcF9vxo7jzwI1uE2Ahg5hiQNCazR60tQrTYJAwnAIvls4U9UivsApAugdSK1QSOubixHWDV29uB6WM1e5SP9aUCwwbesvX9Ffjqa2DvIq8YVm68vHKxeTvhubIKmDyUDVOm9Xle/0iQYOxlizC9iw7Uuv8EwkeaOg/8KXKrp6YB30sKJweulN/MvTIMyYdaco86dFEz4IKig3WOAvxk8LCGkEDLDvgFLM/FIvL3QJNfC77ra0B8Vd5NQNxX8rA02nJZTWDWG7wTNrkRkNxEZApaikwBAfZrIIBcLA72AFq9D+SM5tWu3PEgwHKaIXnYmUDYACCkly2RWy0kRe11LLXFdbasohpStojnYnF2LdC2o08VGGxeAgKr/abLLFtkVDO0VruYhPxpHhrIapeZU8BVYLBxNrB0kohj7WSLaPMVpfV28YZEozCcXjyQoYEv2SIsHQgCqz4dkboOzJ0H1D3rD7AkqoGoMSw0kCowWef7IymmOWoNn9FNeX3oSOA8seYNB3lX6WEvK3yzFQhgpZelcICFBAKwotrFCdoOgCUPe+/f/gFLQhlpnYSH9VPt8gFYrrPlLFskARtI8YCLajyebJHmZS2pLQ5Yc/HAXmdLahQEJluk9Xc55GL1lm+92uVTVMPP/FkpDGcErFPxgPgEpI2afLAFkg58DqXTYu5Vr+9XdLASaCVwfQFWprTsQgJbwP5WlGdf4B6W+K/Mw9bg3QX3anEPSzRDBtgGnMjNUls+crEZluJB5gAuSsw0Cqg8O1J4WP8qMFxni3cemHKxrNrlUDwQityPogKjeVmDzpYjYG3iWN+A9ebF6qktfWByhQOWbbwofuUxrFHg2J+EvFG2KONsHyRFN0J4mCihMq5f99kcnBKgEqzytQwNyLtaPWwggJUbL1aeJcD+gXcVUAuMBOzdxwSsLB7IapcdYLMpJHgMwNoVD4p3+pCQD0y2SAOs+9wjimqUT7bIJ2ALzMUDXStWhgR2ohrbeByrjfL07jwwAtZOQl6qwBQmhyDtZBckRDXAzd2f6d6UAVVRlF/2n4RCmtwnwWn0psbbvtGDGj0ngZABkBhY0lsG8VhU85KU/KcYVFSq0tpyzSsJsoy+nLBC3lATv6ABGBPFxmgG+C2b8qQy5URdrVQy3WAQaiPwUDwpiChMxp30WoXOleuCVlFyl51mZU5XyQk2sM1VdISNwSwriEFp/n6UPtyHkrw9KLm/C8W5O1CUsx1F2VtRmBWBosxNKEzfgIK0MDbIOD9lJR4mr8DDxFDk3VuCBwkhyLu7CA/iFuB+7Fzk3pmN3FvTkXNzKrJvTEb2tQnIujoemVfGIuPSD2xad8aFYUg/NwTpZwch7cwApJ7qh5STvZF6vCdSjnVF0tHOSDrcAUmH2iHxYBvcO/AlEva3RPy+ZoiP+gzxuxvj7q6GiNv5IeIiP0Ds9vdwZ+u7uL3lbdyKCMLt8Oq4tbEKbm6ohBthL+H6ur/i+prncW31f+Lqyl/h6s/P4MpyBVeWKbi8RMGlxQouBiu4GMKf03uXl/L/vxqq4OoKBddWKLi68hlcW/Ucrq/5Ha6v/QNurHseN8NewM31f8OtDS/i9saXcXtTJdwJr4LYzdVwZ0sNxG4NQty2txC3vRbuRtZG/I66iN/5PhL21MeNnY3w614L+C1fgtT4OGbJp0g/KuLVp2B9NLAm/R8Ea4wPsO54VLAq/9fAenbzJ+g/Q7RIG4D5vwFJqydeYin2FQAAAABJRU5ErkJggg==)
</center>