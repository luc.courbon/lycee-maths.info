#Chapitre 1 - Variables, Types et Opérateurs
<center>
``` python
print("Hello World")
```
</center>

##1 - Le type `int` (integer : nombres entiers)

Pour affecter (on dit aussi assigner) la valeur 17 à la variable `age`:

``` python
age=17
```

La fonction print affiche la valeur de la variable :

``` python
print(age)
age
```

La fonction `type()` retourne le type de la variable :

``` python
print(type(age))
```

`int` est le type des nombres entiers.

Si on commence une ligne avec le symbole #, alors la ligne n'est pas interprétée en Python. On utilise les commentaires pour être lu, pour expliquer ce que l'on fait ou pour pouvoir facilement reprendre son travail.

``` python
#Ceci est un commentaire
age=age+1
print(age)
```

``` python
age=age-3
print(age)
```

``` python
age=age*2
print(age)
```

``` python
a=6*3-20
print(a)
```

``` python
b=25
c=a+2*b
print(b,c)
```

L'opérateur `//` donne la division entière (division euclidienne) :

``` python
d=450//360
print(d)
```

L'opérateur `%` donne le reste de la division (opération modulo) :

``` python
reste=450%360
print(reste)
```

L'opérateur `**` donne la puissance :

``` python
Mo=2**20
print(Mo)
```

##2 - Le type `float` (nombres en virgule flottante)

``` python
b=17.0  #le séparateur est un point ! (et pas une virgule).
print(b)
a=17
a==b
```

``` python
print(type(b))
```

``` python
c=14.0/3.0
print(c)
```

``` python
c=14.0//3.0   #C'est la division entière (le quotient dans la division euclidienne)
print(c)
```

Attention : avec des nombres entiers, l'opérateur `/` fait aussi une division entière :

``` python
c=14//3
print(c)
```

Notation scientifique:

``` python
a=-1.784892e4
print(a)
```

**Les fonctions mathématiques**

Pour utiliser les fonctions mathématiques, il faut commencer par
importer le module `math` :

``` python
import math
```

La fonction `dir(module)` retourne la liste des fonctions et données d\'un
module :

``` python
dir(math)
```

Pour appeler une fonction d'un module, la syntaxe est la suivante : **module.fonction(arguments)**

Pour accéder à une donnée d'un module : **module.donnée**

``` python
print(math.pi)		# donnée pi du module math (nombre pi)
```

``` python
print(math.sin(math.pi/4.0)) # fonction sin() du module math (sinus)
```

``` python
print(math.sqrt(2.0)) 	# fonction sqrt() du module math (racine carrée)
```

``` python
print(math.sqrt(-5.0))
```

``` python
print(math.exp(-3.0)) 	# fonction exp() du module math
```

``` python
print(math.log(math.e)) 	# fonction log() du module math (logarithme népérien)
```

##3 - Le type `str` (string : chaîne de caractères)

``` python
nom = 'Dupont' 		# entre apostrophes
print(nom)
```

``` python
print(type(nom))
```

``` python
prenom = "Pierre" 		# on peut aussi utiliser les guillemets
print(prenom)
```

``` python
print(nom,prenom) 		# ne pas oublier la virgule
```

La concaténation désigne la mise bout à bout de plusieurs chaînes de caractères.

La concaténation utilise l'opérateur `+` .

``` python
chaine = nom + prenom 	# concaténation de deux chaînes de caractères
print(chaine)
```

``` python
chaine = prenom + nom 	# concaténation de deux chaînes de caractères
print(chaine)
```

``` python
chaine=prenom+' '+nom
print(chaine)
```

``` python
chaine = chaine + ' 18 ans'	  # en plus court : chaine += '18 ans'
print(chaine)
```

La fonction len() retourne la longueur (length) de la chaîne de caractères :

``` python
print(len(chaine))
```

``` python
print(chaine[0])	# premier caractère (indice 0)
```

``` python
print(chaine[1])	# deuxième caractère (indice 1)
```

``` python
print(chaine[1:4])
```

``` python
print(chaine[2:])
```

``` python
print(chaine[-1])	# dernier caractère (indice -1)
```

``` python
print(chaine[-6:])
```

``` python
chaine='Aujourd'hui'
```

``` python
chaine  = 'Aujourd\'hui'		# séquence d'échappement \'
print(chaine)
```

``` python
chaine = "Aujourd'hui"
print(chaine)
```

La séquence d'échappement `\n` représente un saut ligne :

``` python
chaine = 'Première ligne\nDeuxième ligne'
print(chaine)
```

Plus simplement (ou pas), on peut utiliser les triples guillements (ou les triples apostrophes) pour encadrer une chaîne définie sur plusieurs lignes:

``` python
chaine="""Première ligne
Deuxième ligne"""
print(chaine)
```

On ne peut pas mélanger les serviettes et les torchons (ici type `str` et
type `int`) :

``` python
chaine='17.45'
print(type(chaine))
print(type(float(chaine)))
```

``` python
chaine=chaine+'2'
print(chaine)
```

La fonction `float()` permet de convertir un type str en type float.

``` python
nombre = float(chaine)
print(nombre)
```

``` python
print(type(nombre))
```

``` python
nombre = nombre + 2		# en plus court : nombre += 2
print(nombre)
```

La fonction `input()` lance une invite de commande (en anglais : `prompt`) pour saisir une chaîne de caractères.

``` python
# saisir une chaîne de caractères et valider avec la touche Enter
chaine = input("Entrer un nombre : ")
```

``` python
print(chaine)
```

``` python
print(type(chaine))
```

``` python
nombre = float(chaine)	# conversion de type
print(nombre**2)
```

##4 - Le type `list` (liste) Une liste est une structure de données. Le premier élément d'une liste possède l'indice (l'index) 0. Dans une liste, on peut avoir des éléments de plusieurs types.

Ici, la liste `infoperso` contient 5 éléments de types `str`, `str`, `int`, `float` et `float`:

``` python
infoperso = ['Pierre' , 'Dupont' , 17 , 1.75 , 72.5]
print(type(infoperso))
```

``` python
print(infoperso)
```

``` python
print('Prénom : ',infoperso[0])   # premier élément (indice 0)
```

``` python
print('Age : ',infoperso[2])      # le troisième élément a l'indice 2
```

``` python
print('Taille : ',infoperso[3])		# le quatrième élément a l'indice 3
```

La fonction `range()` crée une liste d'entiers régulièrement espacés :

``` python
maliste = range(10)
print(maliste)
```

``` python
print(type(maliste))
```

``` python
maliste = range(1,10,2)		# range(début,fin non comprise,intervalle)
print(list(maliste))
```

``` python
print(maliste[2])
```

On peut créer une liste de listes, qui s'apparente à un tableau à 2 dimensions (ligne, colonne) :
<center>

|0|1|2|
|:--:|:--:|:--:|
|10|11|12|
|20|21|22|

</center>

``` python
maliste = [[0,1,2],[10,11,12],[20,21,22]]
print(maliste[0])
```

``` python
print(maliste[0][0])
```

``` python
print(maliste[2][1])		# élément à la troisième ligne et deuxième colonne
```

``` python
maliste[2][1] = 69		# nouvelle affectation
print(maliste)
```

##5 - Le type bool (booléen)

Deux valeurs sont possibles : `True` et `False`

``` python
a = True
print(type(a))
```

Les opérateurs de comparaison :

|Opérateur|Signification|Remarque|
|-----|-----|-----|
|<|strictement inférieur|| 
|<=|inférieur ou égal||   
|>|supérieur||        
|>=|supérieur ou égal||    
|==|égal|Attention : deux signes ==|
|!=|différent||        
|is|identique|Deux conditions: égal et même type|
|is not|non identique||      

``` python
a=12
b=12.0
a==b
a is b
```

``` python
b=10
print(b>8)
```

``` python
print(b==5)
```

``` python
print(b!=10)
```

``` python
print(0<=b<=20)
```

Les opérateurs logiques : `and`, `or`, `not`.

``` python
note=13
mentionab = note>=12.0 and note<14.0 # ou bien : mentionab = 12.0 <= note < 14.0
print(mentionab)
```

``` python
print(not mentionab)
```

``` python
print(note==20.0 or note==0.0)
```

L'opérateur `in` s'utilise avec des chaînes (type `str`) ou des listes (type `list`) :

``` python
chaine = 'Bonsoir'
#  la sous-chaîne 'soir' fait-elle partie de la chaîne 'Bonsoir' ?
a = 'soir' in chaine
print(a)
```

``` python
print('b' in chaine)
```

``` python
maliste = [4,8,15]
#  le nombre entier 9 est-il dans la liste ?
print(9 in maliste)
```

``` python
print(8 in maliste)
```

``` python
print(14 not in maliste)
```

##6 - Le type `dict` (dictionnaire)

Un dictionnaire stocke des données sous la forme clé ⇒ valeur. Une clé est unique et n'est pas nécessairement un entier (comme c'est le cas de l'indice d'une liste).

``` python
moyenne = {'math':12.5,'anglais':15.8}	# entre accolades
print(type(moyenne))
```

``` python
print(moyenne['anglais'])
```

``` python
print(moyenne)
```

``` python
moyenne['sport']=11.0
print(moyenne)
```

##7 - Autres types

Nous avons vu les types les plus courants. Il en existe bien d'autres :

- `long` (nombres entiers de longueur quelconque, par exemple 4284961775562012536954159102L)
- `complex` (nombres complexes, par exemple 1+2.5j)
- `tuple` (structure de données)
- `set` (structure de données)
- `file` (fichiers)
- \...

##Exercices

###Exercice 1.1 ☆

Afficher la taille en octets et en bits d\'un fichier de 536 ko.

On donne : 1 ko (1 kilooctet) = $2^{10}$ octets !!!

1 octet = 1 byte = 8 bits

``` python
```

###Exercice 1.2 ★

Le numéro de sécurité sociale est constitué de 13 chiffres auquel s'ajoute la clé de contrôle (2 chiffres).

Exemple : 1 89 11 26 108 268 **

La clé de contrôle est calculée par la formule : 97 - (numéro de sécurité sociale modulo 97)

Retrouver la clé de contrôle du numéro de sécurité sociale donné ci-dessus. Quel est l'intérêt de la clé de contrôle ?

``` python
```

###Exercice 1.3 ★

Afficher la valeur numérique de la racine carrée de (4,63 - 15/16)

Comparer avec votre calculette.

``` python
``` 

###Exercice 1.4 ★

A partir des deux variables Prenom et Nom, afficher les initiales (par exemple LM pour Léa Martin).

``` python
```

###Exercice 1.5 ★☆

L'identifiant d'accès au réseau du lycée est construit de la manière suivante : initiale du prénom puis les 8 premiers caractères du nom (le tout en minuscule).

Exemple : Alexandre Lecouturier → alecoutur

A partir des deux variables Prenom et Nom, construire l'identifiant.

``` python
```

###Exercice 1.6 ★★★

On donne un extrait des logins d'accès au réseau du lycée :

admin pinkzup37

lmartin sloub6074

glahlou gplgnu6074

1.  Créer une variable de type dict qui contient les couples identifiant - mot de passe ci-dessus.
2.  La saisie du login fournit deux variables Identifiant et Motdepasse: une pour l'identifiant et l'autre pour le mot de passe.

Construire une variable booléenne qui donne `True` en cas d'identification correcte, et `False` dans le cas contraire :

lmartin sloub6074 → `True`

lmartin sloub7074 → `False`

martin sloub6074 → `False` (ce cas est plus compliqué à traiter)

``` python
```